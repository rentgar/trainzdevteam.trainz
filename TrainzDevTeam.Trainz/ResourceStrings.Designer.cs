﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан программой.
//     Исполняемая версия:4.0.30319.42000
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TrainzDevTeam.Trainz {
    using System;
    
    
    /// <summary>
    ///   Класс ресурса со строгой типизацией для поиска локализованных строк и т.д.
    /// </summary>
    // Этот класс создан автоматически классом StronglyTypedResourceBuilder
    // с помощью такого средства, как ResGen или Visual Studio.
    // Чтобы добавить или удалить член, измените файл .ResX и снова запустите ResGen
    // с параметром /str или перестройте свой проект VS.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "17.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class ResourceStrings {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal ResourceStrings() {
        }
        
        /// <summary>
        ///   Возвращает кэшированный экземпляр ResourceManager, использованный этим классом.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("TrainzDevTeam.Trainz.ResourceStrings", typeof(ResourceStrings).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Перезаписывает свойство CurrentUICulture текущего потока для всех
        ///   обращений к ресурсу с помощью этого класса ресурса со строгой типизацией.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Такой объект уже существует..
        /// </summary>
        internal static string Exception_AlreadyExist {
            get {
                return ResourceManager.GetString("Exception.AlreadyExist", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Данные не являются данными конфигурации и не могут быть прочитаны..
        /// </summary>
        internal static string Exception_BadConfigurationDataFormat {
            get {
                return ResourceManager.GetString("Exception.BadConfigurationDataFormat", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Необходимо указать хотя бы один файл, содержащий элемент конфигурации.
        /// </summary>
        internal static string Exception_BuilderEmptyPartFiles {
            get {
                return ResourceManager.GetString("Exception.BuilderEmptyPartFiles", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Поток не поддерживает запись.
        /// </summary>
        internal static string Exception_CannotWriteStream {
            get {
                return ResourceManager.GetString("Exception.CannotWriteStream", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Коллекция была изменена; невозможно выполнить операцию перечисления..
        /// </summary>
        internal static string Exception_CollectionFailedVersion {
            get {
                return ResourceManager.GetString("Exception.CollectionFailedVersion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Элемент конфигурации  с наименование &quot;{0}&quot; уже существует в коллекции..
        /// </summary>
        internal static string Exception_ConfigurationItemAlreadyExist {
            get {
                return ResourceManager.GetString("Exception.ConfigurationItemAlreadyExist", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Индекс значения должен находится в пределах от 0 до 255..
        /// </summary>
        internal static string Exception_ConfigurationItemArrayIndex {
            get {
                return ResourceManager.GetString("Exception.ConfigurationItemArrayIndex", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Элемент конфигурации должен содержать от 1 до 255 значений..
        /// </summary>
        internal static string Exception_ConfigurationItemArrayValue {
            get {
                return ResourceManager.GetString("Exception.ConfigurationItemArrayValue", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Наименование элемента конфигурации содержит недопустимые символы, или превышает допустимую длину..
        /// </summary>
        internal static string Exception_ConfigurationKeyName {
            get {
                return ResourceManager.GetString("Exception.ConfigurationKeyName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Данные конфигурации доступны только для чтения..
        /// </summary>
        internal static string Exception_ConfigurationReadOnly {
            get {
                return ResourceManager.GetString("Exception.ConfigurationReadOnly", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Перечисление не запущено или уже завершено..
        /// </summary>
        internal static string Exception_EnumerableCantHappen {
            get {
                return ResourceManager.GetString("Exception.EnumerableCantHappen", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Данные конфигурации содержат ошибку и не могут быть прочитаны..
        /// </summary>
        internal static string Exception_ErrorConfigurationDataFormat {
            get {
                return ResourceManager.GetString("Exception.ErrorConfigurationDataFormat", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Индекс выходит за границы коллекции..
        /// </summary>
        internal static string Exception_IndexOutOfRange {
            get {
                return ResourceManager.GetString("Exception.IndexOutOfRange", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Попытка установить позицию за пределами потока..
        /// </summary>
        internal static string Exception_IndexOutOfStream {
            get {
                return ResourceManager.GetString("Exception.IndexOutOfStream", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Идентификатор автора не может быть отрицательным..
        /// </summary>
        internal static string Exception_KuidAuthorIdCannotNegative {
            get {
                return ResourceManager.GetString("Exception.KuidAuthorIdCannotNegative", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Указанная строка не является действительным KUID номером..
        /// </summary>
        internal static string Exception_KuidFormat {
            get {
                return ResourceManager.GetString("Exception.KuidFormat", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Номер версии должен быть в пределах от 0 до 127..
        /// </summary>
        internal static string Exception_KuidVersion {
            get {
                return ResourceManager.GetString("Exception.KuidVersion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Сумма значений аргументов {0} и {1} больше длины буфера..
        /// </summary>
        internal static string Exception_OffsetCountOutOfRange {
            get {
                return ResourceManager.GetString("Exception.OffsetCountOutOfRange", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Количество обработанных элементов не может быть больше, чем общее количество элементов..
        /// </summary>
        internal static string Exception_ProcessedExceedsCount {
            get {
                return ResourceManager.GetString("Exception.ProcessedExceedsCount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Объект доступен только для чтения..
        /// </summary>
        internal static string Exception_ReadOnly {
            get {
                return ResourceManager.GetString("Exception.ReadOnly", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Один из сохранённых элементов конфигурации не является информацией об активе.
        /// </summary>
        internal static string Exception_RecurringAssetInBuilder {
            get {
                return ResourceManager.GetString("Exception.RecurringAssetInBuilder", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Поток не поддерживает данное действие..
        /// </summary>
        internal static string Exception_StreamOperationNotSupport {
            get {
                return ResourceManager.GetString("Exception.StreamOperationNotSupport", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Значение не может быть отрицательным..
        /// </summary>
        internal static string Exception_ValueCannotNegative {
            get {
                return ResourceManager.GetString("Exception.ValueCannotNegative", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Объект должен иметь тип {0}..
        /// </summary>
        internal static string Exception_WrongArgumentType {
            get {
                return ResourceManager.GetString("Exception.WrongArgumentType", resourceCulture);
            }
        }
    }
}
