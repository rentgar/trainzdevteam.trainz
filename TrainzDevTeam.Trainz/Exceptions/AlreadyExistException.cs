﻿using System;

namespace TrainzDevTeam.Trainz
{
    /// <summary>
    /// Это исключение выбрасывается, если добавляемый объект уже существует.
    /// </summary>
    public class AlreadyExistException : Exception
    {

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="AlreadyExistException"/>.
        /// </summary>
        public AlreadyExistException() : base(ResourceStrings.Exception_AlreadyExist) { }

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="AlreadyExistException"/>.
        /// </summary>
        /// <param name="message">Сообщение о причине возникноваения исключения.</param>
        public AlreadyExistException(string message) : base(message) { }

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="AlreadyExistException"/>.
        /// </summary>
        /// <param name="message">Сообщение о причине возникноваения исключения.</param>
        /// <param name="innerException">Исключение, вызвавшее текущее исключение, или значение <see langword="null"/>, если внутренее исключение не задано.</param>
        public AlreadyExistException(string message, Exception innerException) : base(message, innerException) { }

    }

}
