﻿using System;

namespace TrainzDevTeam.Trainz
{
    public class BadFormatException : Exception 
    {

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="BadFormatException"/>.
        /// </summary>
        /// <param name="message">Сообщение о причине возникноваения исключения.</param>
        public BadFormatException(string message) : base(message) { }

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="BadFormatException"/>.
        /// </summary>
        /// <param name="message">Сообщение о причине возникноваения исключения.</param>
        /// <param name="innerException">Исключение, вызвавшее текущее исключение, или пустая ссылка (Nothing в Visual Basic), если внутренее исключение не задано.</param>
        public BadFormatException(string message, Exception innerException) : base(message, innerException) { }

    }
}
