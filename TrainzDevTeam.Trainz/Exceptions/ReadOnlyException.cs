﻿using System;

namespace TrainzDevTeam.Trainz
{
    /// <summary>
    /// Это исключение выбрасывается при попытке внесение изменения в объект доступный только для чтения.
    /// </summary>
    public class ReadOnlyException : Exception
    {

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="ReadOnlyException"/>.
        /// </summary>
        /// <param name="message">Сообщение о причине возникноваения исключения.</param>
        public ReadOnlyException(string message) : base(message) { }

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="ReadOnlyException"/>.
        /// </summary>
        /// <param name="message">Сообщение о причине возникноваения исключения.</param>
        /// <param name="innerException">Исключение, вызвавшее текущее исключение, или пустая ссылка (Nothing в Visual Basic), если внутренее исключение не задано.</param>
        public ReadOnlyException(string message, Exception innerException) : base(message, innerException) { }

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="ReadOnlyException"/> с сообщением по умолчанию.
        /// </summary>
        public ReadOnlyException() : base(ResourceStrings.Exception_ReadOnly) { }

    }

}
