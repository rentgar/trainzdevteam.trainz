﻿using System;

namespace TrainzDevTeam.Trainz.Meshes
{

    /// <summary>
    /// Описывает точку в трехмерном пространстве.
    /// </summary>
    public struct Point3D : IEquatable<Point3D>
    {

        #region Конструктор

        /// <summary>
        /// Инициализирует новый экземпляр структуры <see cref="Point3D"/>.
        /// </summary>
        /// <param name="x">Координата точки по оси X.</param>
        /// <param name="y">Координата точки по оси Y.</param>
        /// <param name="z">Координата точки по оси Z.</param>
        public Point3D(float x, float y, float z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        #endregion

        #region Методы

        #region Общие

        /// <summary>
        /// Добавляет указанный <see cref="Vector3D"/> к указанному <see cref="Point3D"/>.
        /// </summary>
        /// <param name="point">Объект <see cref="Point3D"/>, к которому необходимо добавить указанный <see cref="Vector3D"/>.</param>
        /// <param name="vector">Добавляемый объект <see cref="Vector3D"/>.</param>
        /// <returns><see cref="Point3D"/>, явяляющийс результатом сложения.</returns>
        public static Point3D Add(Point3D point, Vector3D vector) => new Point3D(point.X + vector.X, point.Y + vector.Y, point.Z + vector.Z);

        /// <summary>
        /// Возвращает результат вычитания указаного <see cref="Vector3D"/> из указаного <see cref="Point3D"/>.
        /// </summary>
        /// <param name="point">Объект <see cref="Point3D"/> из которого необходимо вычесть указанный <see cref="Vector3D"/>.</param>
        /// <param name="vector">Вычитаемый объект <see cref="Vector3D"/>.</param>
        /// <returns><see cref="Point3D"/>, являющийся результатом вычитания.</returns>
        public static Point3D Subtract(Point3D point, Vector3D vector) => new Point3D(point.X - vector.X, point.Y - vector.Y, point.Z - vector.Z);

        #endregion

        #region Внешние

        /// <summary>
        /// Перемещает координату точки на указанную велечину.
        /// </summary>
        /// <param name="point">Велечина смещения текущего <see cref="Point3D"/>, заданое другим <see cref="Point3D"/>.</param>
        public void Offset(Point3D point) => Offset(point.X, point.Y, point.Z);

        /// <summary>
        /// Перемещает координату точки на указанную велечину.
        /// </summary>
        /// <param name="dx">Смещение коорлинаты по оси X.</param>
        /// <param name="dy">Смещение коорлинаты по оси Y.</param>
        /// <param name="dz">Смещение коорлинаты по оси Z.</param>
        public void Offset(float dx, float dy, float dz)
        {
            X += dx;
            Y += dy;
            Z += dz;
        }

        /// <summary>
        /// Преобразует текущий объект <see cref="Point3D"/> в формат, доступный для чтения.
        /// </summary>
        /// <param name="format">Формат, в который должен быть преобразован объект: {0} - X; {1} - Y; {2} - Z.</param>
        /// <returns>Строка, представляющая текущий объект <see cref="Point3D"/>, в формате заданном в <paramref name="format"/>.</returns>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="format"/> не может быть <see langword="null"/>.</exception>
        /// <exception cref="FormatException">Недопустимое значение <paramref name="format"/>. -или- Индекс элемента ниже 0 (нуля) или больше 2 (двух).</exception>
        public string ToString(string format)
        {
            if (format == null) throw new ArgumentNullException(format);
            return string.Format(format, X, Y, Z);
        }

        /// <summary>
        /// Преобразует текущий объект <see cref="Point3D"/> в формат, доступный для чтения.
        /// </summary>
        /// <returns>Строка, представляющая текущий объект <see cref="Point3D"/>.</returns>
        public override string ToString() => ToString(@"{{X={0}, Y={1}, Z={2}}}");

        /// <summary>
        /// Определяет, равен ли этот <see cref="Point3D"/> заданному <see cref="Point3D"/>.
        /// </summary>
        /// <param name="point"><see cref="Point3D"/> для сравнения с текущим <see cref="Point3D"/>.</param>
        /// <returns>Значение true, если указанный <see cref="Point3D"/> равен текущему <see cref="Point3D"/>; в противном случае — значение false.</returns>
        public bool Equals(Point3D point) => point.X == X && point.Y == Y && point.Z == Z;

        /// <summary>
        /// Определяет, равен ли этот <see cref="Point3D"/> заданному объекту.
        /// </summary>
        /// <param name="obj">Объект для сравнения с текущим <see cref="Point3D"/>.</param>
        /// <returns>Значение true, если указанный объект равен текущему <see cref="Point3D"/>; в противном случае — значение false.</returns>
        public override bool Equals(object obj) => obj != null && obj is Point3D && Equals((Point3D)obj);

        /// <summary>
        /// Возвращает хэш-код текущего <see cref="Point3D"/>.
        /// </summary>
        /// <returns>Хэш-код для текущего объекта.</returns>
        public override int GetHashCode() => X.GetHashCode() ^ Y.GetHashCode() ^ Z.GetHashCode();

        #endregion

        #endregion

        #region Свойства

        /// <summary>
        /// Получает или задаёт координату точки по оси X.
        /// </summary>
        public float X { get; set; }

        /// <summary>
        /// Получает или задаёт координату точки по оси Y.
        /// </summary>
        public float Y { get; set; }

        /// <summary>
        /// Получает или задаёт координату точки по оси Z.
        /// </summary>
        public float Z { get; set; }

        /// <summary>
        /// Возвращает значение, указывающее, имеет ли текущий <see cref="Point3D"/>, значения <see cref="X"/>, <see cref="Y"/>, <see cref="Z"/> равными 0.
        /// </summary>
        public bool IsEmpty => X == 0 && Y == 0 && Z == 0;

        /// <summary>
        /// Получает объект <see cref="Point3D"/>, у которого значения <see cref="X"/>, <see cref="Y"/>, <see cref="Z"/> равны 0.
        /// </summary>
        public static Point3D Empty => new Point3D();

        #endregion

        #region Операторы

        /// <summary>
        /// Сравнивает два объекта <see cref="Point3D"/>.
        /// Результат указывает, равны ли значения <see cref="X"/>, <see cref="Y"/> и <see cref="Z"/> этих двух объектов <see cref="Point3D"/>.
        /// </summary>
        /// <param name="one">Первый объект <see cref="Point3D"/> для сравнения.</param>
        /// <param name="two">Второй объект <see cref="Point3D"/> для сравнения.</param>
        /// <returns>Значение true, если значения свойств <see cref="X"/>, <see cref="Y"/> и <see cref="Z"/> объектов <paramref name="one"/> и <paramref name="two"/> равны; в противном случае — значение false.</returns>
        public static bool operator ==(Point3D one, Point3D two) => one.Equals(two);

        /// <summary>
        /// Сравнивает два объекта <see cref="Point3D"/>.
        /// Результат указывает, не равны ли значения <see cref="X"/>, <see cref="Y"/> или <see cref="Z"/> этих двух объектов <see cref="Point3D"/>.
        /// </summary>
        /// <param name="one">Первый объект <see cref="Point3D"/> для сравнения.</param>
        /// <param name="two">Второй объект <see cref="Point3D"/> для сравнения.</param>
        /// <returns>Значение true, если хотя бы одно из значений свойства <see cref="X"/>, <see cref="Y"/> или <see cref="Z"/> объектов <paramref name="one"/> и <paramref name="two"/> не равно; в противном случае — значение false.</returns>
        public static bool operator !=(Point3D one, Point3D two) => !one.Equals(two);

        /// <summary>
        /// Добавляет указанный <see cref="Vector3D"/> к указанному <see cref="Point3D"/>.
        /// </summary>
        /// <param name="point">Объект <see cref="Point3D"/>, к которому необходимо добавить указанный <see cref="Vector3D"/>.</param>
        /// <param name="vector">Добавляемый объект <see cref="Vector3D"/>.</param>
        /// <returns><see cref="Point3D"/>, явяляющийс результатом сложения.</returns>
        public static Point3D operator +(Point3D point, Vector3D vector) => Add(point, vector);

        /// <summary>
        /// Возвращает результат вычитания указаного <see cref="Vector3D"/> из указаного <see cref="Point3D"/>.
        /// </summary>
        /// <param name="point">Объект <see cref="Point3D"/> из которого необходимо вычесть указанный <see cref="Vector3D"/>.</param>
        /// <param name="vector">Вычитаемый объект <see cref="Vector3D"/>.</param>
        /// <returns><see cref="Point3D"/>, являющийся результатом вычитания.</returns>
        public static Point3D operator -(Point3D point, Vector3D vector) => Subtract(point, vector);

        /// <summary>
        /// Вычисляет вектор между двумя точками <see cref="Point3D"/>.
        /// </summary>
        /// <param name="one">Первая точка <see cref="Point3D"/>.</param>
        /// <param name="two">Вторая точка <see cref="Point3D"/>.</param>
        /// <returns>Объект <see cref="Vector3D"/>, представляющий вектор между двумя точками.</returns>
        public static Vector3D operator -(Point3D one, Point3D two) => new Vector3D(one.X - two.X, one.Y - two.Y, one.Z - two.Z);

        /// <summary>
        /// Преобразует точку в вектор.
        /// </summary>
        /// <param name="point">Преобразуемая точка <see cref="Point3D"/>.</param>
        public static explicit operator Vector3D(Point3D point) => new Vector3D(point.X, point.Y, point.Z);

        #endregion

    }

}
