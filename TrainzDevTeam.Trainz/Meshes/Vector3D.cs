﻿using System;

namespace TrainzDevTeam.Trainz.Meshes
{
    /// <summary>
    /// Описывает твектор в трехмерном пространстве.
    /// </summary>
    public struct Vector3D : IEquatable<Vector3D>
    {

        #region Конструктор

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="Vector3D"/>.
        /// </summary>
        /// <param name="x">Величина вектора по оси X.</param>
        /// <param name="y">Величина вектора по оси Y.</param>
        /// <param name="z">Величина вектора по оси Z.</param>
        public Vector3D(float x, float y, float z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        #endregion

        #region Метод

        /// <summary>
        /// Выполняет сложение двух указанных векторов.
        /// </summary>
        /// <param name="one">Вектор, к которому необходим прибавить <paramref name="two"/>.</param>
        /// <param name="two">Вектор, который пребавляется к <paramref name="one"/>.</param>
        /// <returns>Вектор <see cref="Vector3D"/>, являющийся суммой векторов <paramref name="one"/> и <paramref name="two"/>.</returns>
        public static Vector3D Add(Vector3D one, Vector3D two) => new Vector3D(one.X + two.X, one.Y + two.Y, one.Z + two.Z);

        /// <summary>
        /// Выполняет расчёт разности двух векторов.
        /// </summary>
        /// <param name="one">Вектор, из которого выполняется вычитание.</param>
        /// <param name="two">Вектор, который вычитается из вектора <paramref name="one"/>.</param>
        /// <returns>Вектор <see cref="Vector3D"/>, являющийся разностью векторов <paramref name="one"/> и <paramref name="two"/>.</returns>
        public static Vector3D Subtract(Vector3D one, Vector3D two) => new Vector3D(one.X - two.X, one.Y - two.Y, one.Z - two.Z);

        /// <summary>
        /// Произведение вектора на число.
        /// </summary>
        /// <param name="vector">Вектор, который необходимо умножить на число.</param>
        /// <param name="scalar">Число, на которое необходимо умножить вектор.</param>
        /// <returns>Вектор <see cref="Vector3D"/>, являющийся результатом произведения вектора <paramref name="vector"/> на <paramref name="scalar"/>.</returns>
        public static Vector3D Multiply(Vector3D vector, float scalar) => new Vector3D(vector.X * scalar, vector.Y * scalar, vector.Y * scalar);

        /// <summary>
        /// ВЫполняет векторное произведение двух векторов.
        /// </summary>
        /// <param name="one">Первый аргумент произведения.</param>
        /// <param name="two">Второй аргумент произведения.</param>
        /// <returns>Вектор <see cref="Vector3D"/>, являющийся результатом произведения вектора <paramref name="one"/> на <paramref name="two"/>.</returns>
        public static Vector3D Multiply(Vector3D one, Vector3D two) => new Vector3D(one.Y * two.Z - one.Z * two.Y, one.Z * two.X - one.X * two.Z, one.X * two.Y - one.Y * two.X);

        /// <summary>
        /// Выполняет скалярное произведение двух векторов.
        /// </summary>
        /// <param name="one">Первый аргумент произведения.</param>
        /// <param name="two">Второй аргумент произведения.</param>
        /// <returns>Результат скалярного произведения векторов.</returns>
        public static float Scalar(Vector3D one, Vector3D two) => one.X * two.X + one.Y * two.Y + one.Z * two.Z;

        /// <summary>
        /// Нормализует указанный вектор.
        /// </summary>
        /// <param name="vector">Вектор, который необходимо нормализовать.</param>
        /// <returns>Нормализованный вектор <see cref="Vector3D"/>.</returns>
        public static Vector3D Normalize(Vector3D vector)
        {
            if (vector.IsNormalize) return vector;
            double magnitude = 1.0 / vector.Length;
            return new Vector3D((float)(vector.X * magnitude), (float)(vector.Y * magnitude), (float)(vector.Z * magnitude));
        }

        /// <summary>
        /// Преобразует текущий объект <see cref="Vector3D"/> в формат, доступный для чтения.
        /// </summary>
        /// <param name="format">Формат, в который должен быть преобразован объект: {0} - X; {1} - Y; {2} - Z.</param>
        /// <returns>Строка, представляющая текущий объект <see cref="Vector3D"/>, в формате заданном в <paramref name="format"/>.</returns>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="format"/> не может быть null</exception>
        public string ToString(string format)
        {
            if (format == null) throw new ArgumentNullException(format);
            return string.Format(format, X, Y, Z);
        }

        /// <summary>
        /// Преобразует текущий объект <see cref="Vector3D"/> в формат, доступный для чтения.
        /// </summary>
        /// <returns>Строка, представляющая текущий объект <see cref="Point3D"/>.</returns>
        public override string ToString() => ToString(@"({0}, {1}, {2})");

        /// <summary>
        /// Определяет, равен ли этот <see cref="Vector3D"/> заданному <see cref="Vector3D"/>.
        /// </summary>
        /// <param name="vector"><see cref="Vector3D"/> для сравнения с текущим <see cref="Vector3D"/>.</param>
        /// <returns>Значение true, если указанный <see cref="Vector3D"/> равен текущему <see cref="Vector3D"/>; в противном случае — значение false.</returns>
        public bool Equals(Vector3D vector) => vector.X == X && vector.Y == Y && vector.Z == Z;

        /// <summary>
        /// Определяет, равен ли этот <see cref="Vector3D"/> заданному объекту.
        /// </summary>
        /// <param name="obj">Объект для сравнения с текущим <see cref="Point3D"/>.</param>
        /// <returns>Значение true, если указанный объект равен текущему <see cref="Point3D"/>; в противном случае — значение false.</returns>
        public override bool Equals(object obj) => obj is Vector3D && Equals((Vector3D)obj);

        /// <summary>
        /// Возвращает хэш-код текущего <see cref="Vector3D"/>.
        /// </summary>
        /// <returns>Хэш-код для текущего объекта.</returns>
        public override int GetHashCode() => X.GetHashCode() ^ Y.GetHashCode() ^ Z.GetHashCode();

        #endregion

        #region Свойства

        /// <summary>
        /// Возвращает значение вектора по оси X.
        /// </summary>
        public float X { get; set; }

        /// <summary>
        /// Возвращает значение вектора по оси Y.
        /// </summary>
        public float Y { get; set; }

        /// <summary>
        /// Возвращает значение вектора по оси Z.
        /// </summary>
        public float Z { get; set; }

        /// <summary>
        /// Получает длину вектора.
        /// </summary>
        public float Length => (float)Math.Sqrt(Math.Pow(X, 2) + Math.Pow(Y, 2) + Math.Pow(Z, 2));

        /// <summary>
        /// Возвращает значение true, если вектор является енормализованным; в противном случае — значение false.
        /// </summary>
        public bool IsNormalize => Length == 1.0f;

        /// <summary>
        /// Возвращает значение true, если вектор является нулевым (свойства <see cref="X"/>, <see cref="Y"/> и <see cref="Z"/> равны нолю); в противном случае — значение false.
        /// </summary>
        public bool IsNull => X == 0 && Y == 0 && Z == 0;

        #endregion

        #region Операторы

        /// <summary>
        /// Сравнивает два объекта <see cref="Vector3D"/>
        /// Результат указывает, равны ли значения <see cref="X"/>, <see cref="Y"/> и <see cref="Z"/> этих двух объектов <see cref="Vector3D"/>.
        /// </summary>
        /// <param name="one">Первый объект <see cref="Vector3D"/> для сравнения.</param>
        /// <param name="two">Второй объект <see cref="Vector3D"/> для сравнения.</param>
        /// <returns>Значение true, если значения свойств <see cref="X"/>, <see cref="Y"/> и <see cref="Z"/> объектов <paramref name="one"/> и <paramref name="two"/> равны; в противном случае — значение false.</returns>
        public static bool operator ==(Vector3D one, Vector3D two) => one.Equals(two);

        /// <summary>
        /// Сравнивает два объекта <see cref="Vector3D"/>.
        /// Результат указывает, не равны ли значения <see cref="X"/>, <see cref="Y"/> или <see cref="Z"/> этих двух объектов <see cref="Vector3D"/>.
        /// </summary>
        /// <param name="one">Первый объект <see cref="Vector3D"/> для сравнения.</param>
        /// <param name="two">Второй объект <see cref="Vector3D"/> для сравнения.</param>
        /// <returns>Значение true, если хотя бы одно из значений свойства <see cref="X"/>, <see cref="Y"/> или <see cref="Z"/> объектов <paramref name="one"/> и <paramref name="two"/> не равно; в противном случае — значение false.</returns>
        public static bool operator !=(Vector3D one, Vector3D two) => !one.Equals(two);

        /// <summary>
        /// Выполняет сложение двух векторов.
        /// </summary>
        /// <param name="one">Вектор, к которому необходим прибавить <paramref name="two"/>.</param>
        /// <param name="two">Вектор, который пребавляется к <paramref name="one"/>.</param>
        /// <returns>Вектор <see cref="Vector3D"/>, являющийся суммой векторов <paramref name="one"/> и <paramref name="two"/>.</returns>
        public static Vector3D operator +(Vector3D one, Vector3D two) => Add(one, two);

        /// <summary>
        /// Выполняет расчёт разности двух векторов.
        /// </summary>
        /// <param name="one">Вектор, из которого выполняется вычитание</param>
        /// <param name="two">Вектор, который вычитается из вектора <paramref name="one"/>.</param>
        /// <returns>Вектор <see cref="Vector3D"/>, являющийся разностью векторов <paramref name="one"/> и <paramref name="two"/>.</returns>
        public static Vector3D operator -(Vector3D one, Vector3D two) => Subtract(one, two);

        /// <summary>
        /// ВЫполняет векторное произведение двух векторов.
        /// </summary>
        /// <param name="one">Первый аргумент произведения.</param>
        /// <param name="two">Второй аргумент произведения.</param>
        /// <returns>Вектор <see cref="Vector3D"/>, являющийся результатом произведения вектора <paramref name="one"/> на <paramref name="two"/>.</returns>
        public static Vector3D operator *(Vector3D one, Vector3D two) => Multiply(one, two);

        /// <summary>
        /// Произведение вектора на число.
        /// </summary>
        /// <param name="vector">Вектор, который необходимо умножить на число.</param>
        /// <param name="scalar">Число, на которое необходимо умножить вектор.</param>
        /// <returns>Вектор <see cref="Vector3D"/>, являющийся результатом произведения вектора <paramref name="vector"/> на <paramref name="scalar"/>.</returns>
        public static Vector3D operator *(Vector3D vector, float scalar) => Multiply(vector, scalar);

        /// <summary>
        /// Преобразует вектор в точку.
        /// </summary>
        /// <param name="vector">Преобразуемый вектор <see cref="Vector3D"/>.</param>
        public static explicit operator Point3D(Vector3D vector) => new Point3D(vector.X, vector.Y, vector.Z);

        #endregion

    }
}
