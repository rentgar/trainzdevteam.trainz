﻿using System.IO;

namespace TrainzDevTeam.Trainz
{
    /// <summary>
    /// Описывает элемент конфигурации неизвестного типа.
    /// </summary>
    public sealed class UnknownConfigurationItem : ConfigurationItem
    {

        #region Конструктор

        internal UnknownConfigurationItem(int size, ConfigurationItemType type, string name, int itemPosition, Stream stream) : base(size, type, name, itemPosition, stream) { }

        #endregion

        #region Свойства

        /// <summary>
        /// Получает размер данных текущего элемента конфигурации в байтах.
        /// </summary>
        public override int DataSize => SourceSize - Name.Length - 3;

        internal override bool CanSave => false;

        #endregion

    }

}
