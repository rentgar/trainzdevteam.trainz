﻿using System.IO;

namespace TrainzDevTeam.Trainz
{
    /// <summary>
    /// Описиывает элемент конфигурации, содержащий данные вложенного в конфигурацию файла.
    /// </summary>
    public sealed class FileConfigurationItem : ConfigurationItem
    {

        #region Конструктор

        internal FileConfigurationItem(int size, ConfigurationItemType type, string name, int itemPosition, Stream stream) : base(size, type, name, itemPosition, stream) => DataStream = new ReadDataStream(stream, (int)stream.Position, DataSize);

        #endregion

        #region Свойства

        /// <summary>
        /// Получение потока для доступа к данным файла.
        /// </summary>
        public Stream DataStream { get; }

        /// <summary>
        /// Получает размер данных текущего элемента конфигурации в байтах.
        /// </summary>
        public override int DataSize => SourceSize - Name.Length - 3;

        internal override bool CanSave => false;

        #endregion

    }

}
