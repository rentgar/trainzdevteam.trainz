﻿using System;
using System.IO;

namespace TrainzDevTeam.Trainz
{
    /// <summary>
    /// Представляет хранилище для единичной секции конфигурации
    /// </summary>
    public sealed class ConfigurationPartialData : IDisposable
    {

        #region Данные класса

        private Stream _stream;
        private bool _disposed = false;

        #endregion

        #region Конструктор

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="ConfigurationPartialData"/>.
        /// </summary>
        /// <param name="path">Абсолютный или относительный путь к файлу, содержащему данные элемента конфигурации.</param>
        /// <exception cref="ArgumentNullException">Аргумент <paramref name="path"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> представляет собой пустую строку (""), содержащую только пробел или хотя бы один недопустимый символ. -или- path ссылается на устройство нефайлового типа, например "con:", "com1:", "lpt1:" и т. д., в среде NTFS.</exception>
        /// <exception cref="NotSupportedException">Аргумент <paramref name="path"/> ссылается на устройство нефайлового типа, например "con:", "com1:", "lpt1:" и т. д., в среде, отличной от NTFS.</exception>
        /// <exception cref="FileNotFoundException">Не удается найти файл, заданный аргументом <paramref name="path"/>.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        /// <exception cref="System.Security.SecurityException"> У вызывающего объекта отсутствует необходимое разрешение.</exception>
        /// <exception cref="DirectoryNotFoundException">Указан недопустимый путь (например, ведущий на несопоставленный диск).</exception>
        /// <exception cref="PathTooLongException">Указанный путь, имя файла или оба значения превышают максимальную длину, заданную в системе. Например, для платформ на основе Windows длина пути должна составлять менее 248 знаков, а длина имен файлов — менее 260 знаков.</exception>
        /// <exception cref="BadFormatException">Неверный формат хранилища.</exception>
        public ConfigurationPartialData(string path) : this(new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read), null, null) { }

        private ConfigurationPartialData(Stream stream, ProgressEventCallback progressCallback, ProgressEventArgs eventArgs)
        {
            try {
                var items = ConfigurationInternalFunctions.ParseConfigurationDataBlock(stream, 0, (int)stream.Length, progressCallback, eventArgs ?? new ProgressEventArgs(stream.Length, 0));
                if (items.Length == 1) Item = items[0];
                else throw new BadFormatException(ResourceStrings.Exception_BadConfigurationDataFormat);
                _stream = stream;
            } catch {
                stream.Dispose();
                throw;
            }
        }

        #endregion

        #region Методы

        #region Статические

        /// <summary>
        /// Сохраняет элемент конфигурации в частичное хранилище.
        /// </summary>
        /// <param name="item">Объект <see cref="IConfigurationItem"/>, содержащий данные элемента конфигурации, который необходимо сохранить.</param>
        /// <param name="path">Имя файла, в который необходимо сохранить элемент конфигурации.</param>
        /// <exception cref="ArgumentNullException">Аргумент <paramref name="path"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> представляет собой пустую строку (""), содержащую только пробел или хотя бы один недопустимый символ. -или- path ссылается на устройство нефайлового типа, например "con:", "com1:", "lpt1:" и т. д., в среде NTFS.</exception>
        /// <exception cref="NotSupportedException">Аргумент <paramref name="path"/> ссылается на устройство нефайлового типа, например "con:", "com1:", "lpt1:" и т. д., в среде, отличной от NTFS.</exception>
        /// <exception cref="FileNotFoundException">Не удается найти файл, заданный аргументом <paramref name="path"/>.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        /// <exception cref="System.Security.SecurityException"> У вызывающего объекта отсутствует необходимое разрешение.</exception>
        /// <exception cref="DirectoryNotFoundException">Указан недопустимый путь (например, ведущий на несопоставленный диск).</exception>
        /// <exception cref="PathTooLongException">Указанный путь, имя файла или оба значения превышают максимальную длину, заданную в системе. Например, для платформ на основе Windows длина пути должна составлять менее 248 знаков, а длина имен файлов — менее 260 знаков.</exception>
        public static void CreatePartialData(ConfigurationItem item, string path) => CreatePartialData(item, path, null);

        /// <summary>
        /// Сохраняет элемент конфигурации в частичное хранилище.
        /// </summary>
        /// <param name="item">Объект <see cref="IConfigurationItem"/>, содержащий данные элемента конфигурации, который необходимо сохранить.</param>
        /// <param name="path">Имя файла, в который необходимо сохранить элемент конфигурации.</param>
        /// <param name="progressCallback">Делегат <see cref="ProgressEventCallback"/>, для обратного вызова сообщений о ходе процесса переноса данных.</param>
        /// <exception cref="ArgumentNullException">Аргумент <paramref name="path"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> представляет собой пустую строку (""), содержащую только пробел или хотя бы один недопустимый символ. -или- path ссылается на устройство нефайлового типа, например "con:", "com1:", "lpt1:" и т. д., в среде NTFS.</exception>
        /// <exception cref="NotSupportedException">Аргумент <paramref name="path"/> ссылается на устройство нефайлового типа, например "con:", "com1:", "lpt1:" и т. д., в среде, отличной от NTFS.</exception>
        /// <exception cref="FileNotFoundException">Не удается найти файл, заданный аргументом <paramref name="path"/>.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        /// <exception cref="System.Security.SecurityException"> У вызывающего объекта отсутствует необходимое разрешение.</exception>
        /// <exception cref="DirectoryNotFoundException">Указан недопустимый путь (например, ведущий на несопоставленный диск).</exception>
        /// <exception cref="PathTooLongException">Указанный путь, имя файла или оба значения превышают максимальную длину, заданную в системе. Например, для платформ на основе Windows длина пути должна составлять менее 248 знаков, а длина имен файлов — менее 260 знаков.</exception>
        public static void CreatePartialData(ConfigurationItem item, string path, ProgressEventCallback progressCallback)
        {
            if (item == null) throw new ArgumentNullException(nameof(item));
            using (var stream = new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.None)) {
                var datasize = item.Size + 4;
                var eventargs = new ProgressEventArgs(datasize, 0);
                progressCallback?.Invoke(eventargs);
                stream.SetLength(datasize);
                ConfigurationInternalFunctions.SaveConfigurationItem(item, stream, progressCallback, eventargs);
            }
        }

        /// <summary>
        /// Создаёт новый объект <see cref="ConfigurationPartialData"/> на основе файла, содержащего данные сохранённого элемента конфигурации.
        /// </summary>
        /// <param name="path">Абсолютный или относительный путь к файлу, содержащему данные элемента конфигурации.</param>
        /// <param name="progressCallback">Делегат <see cref="ProgressEventCallback"/>, для обратного вызова сообщений о ходе процесса разбора данных</param>
        /// <exception cref="ArgumentNullException">Аргумент <paramref name="path"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="path"/> представляет собой пустую строку (""), содержащую только пробел или хотя бы один недопустимый символ. -или- path ссылается на устройство нефайлового типа, например "con:", "com1:", "lpt1:" и т. д., в среде NTFS.</exception>
        /// <exception cref="NotSupportedException">Аргумент <paramref name="path"/> ссылается на устройство нефайлового типа, например "con:", "com1:", "lpt1:" и т. д., в среде, отличной от NTFS.</exception>
        /// <exception cref="FileNotFoundException">Не удается найти файл, заданный аргументом <paramref name="path"/>.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        /// <exception cref="System.Security.SecurityException"> У вызывающего объекта отсутствует необходимое разрешение.</exception>
        /// <exception cref="DirectoryNotFoundException">Указан недопустимый путь (например, ведущий на несопоставленный диск).</exception>
        /// <exception cref="PathTooLongException">Указанный путь, имя файла или оба значения превышают максимальную длину, заданную в системе. Например, для платформ на основе Windows длина пути должна составлять менее 248 знаков, а длина имен файлов — менее 260 знаков.</exception>
        /// <exception cref="BadFormatException">Неверный формат хранилища.</exception>
        public static ConfigurationPartialData OpenFromFile(string path, ProgressEventCallback progressCallback)
        {
            Stream stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read);
            return new ConfigurationPartialData(stream, progressCallback, new ProgressEventArgs(stream.Length, 0));
        }

        #endregion

        #region Внешние

        /// <summary>
        /// Освобождает все ресурсы, занятые с <see cref="ConfigurationData"/>
        /// </summary>
        public void Dispose()
        {
            if (!_disposed) {
                _stream.Dispose();
                _disposed = true;
            }
        }

        #endregion

        #endregion

        #region Свойства

        /// <summary>
        /// Получает тег сохранённый в хранилище.
        /// </summary>
        public ConfigurationItem Item { get; }

        #endregion

    }

}
