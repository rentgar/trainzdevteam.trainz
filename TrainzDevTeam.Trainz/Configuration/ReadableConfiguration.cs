﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace TrainzDevTeam.Trainz
{
    /// <summary>
    /// Базовый класс для описания структуры данных конфигурации.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1710")]
    public abstract class ReadableConfiguration : IConfigurationItem, IConfigurationItemReadOnlyCollection, IReadOnlyList<ConfigurationItem>, IEnumerableList<ConfigurationItem>
    {

        #region Данные класса

        protected private ConfigurationItem[] _items;

        #endregion

        #region Конструктор

        protected private ReadableConfiguration() { }

        #endregion

        #region Методы

        #region Внешние

        /// <summary>
        /// Выполняет поиск элемента конфигурации в коллекции.
        /// </summary>
        /// <param name="itemName">Имя элемента конфигурации, который необходимо найти.</param>
        /// <returns>Объект <see cref="ConfigurationItem"/>, содержащий данные найденого элемента конфигурации; или значение null, если элемент не был найден.</returns>
        /// <exception cref="ArgumentNullException">Аргумент <paramref name="itemName"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="itemName"/> содержит недопустимое значение.</exception>
        public ConfigurationItem FindItem(string itemName)
        {
            if (itemName == null) throw new ArgumentNullException(nameof(itemName));
            if (!ConfigurationItem.ItemNameIsValid(itemName)) throw new ArgumentException(ResourceStrings.Exception_ConfigurationKeyName, nameof(itemName));
            return _items.FirstOrDefault(item => item.Name.Equals(itemName, StringComparison.InvariantCultureIgnoreCase));
        }

        /// <summary>
        /// Выполняет поиск индекса для входящего элемента конфигурации.
        /// </summary>
        /// <param name="item">Объект <see cref="ConfigurationItem"/>, содержащий данные элемента конфигурации индекс которого необходимо найти.</param>
        /// <returns>Отсчитываемый от нуля индекс найденого элемента конфигурации, или значение -1, если жлемент не был найден в коллекции.</returns>
        /// <exception cref="ArgumentNullException">Аргумент <paramref name="item"/> не может быть null.</exception>
        public int IndexOf(ConfigurationItem item)
        {
            if (item == null) throw new ArgumentNullException(nameof(item));
            return ItemIndex(item);
        }

        /// <summary>
        /// Выполняет поиск индекса для входящего элемента конфигурации.
        /// </summary>
        /// <param name="itemName">Наименование тега, который необходимо найти.</param>
        /// <returns>Индекс элемента конфигурации или -1 - если жлемент не был найден.</returns>
        /// <exception cref="ArgumentNullException">Аргумент <paramref name="itemName"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="itemName"/> содержит недопустимое значение.</exception>
        public int IndexOf(string itemName)
        {
            if (itemName == null) throw new ArgumentNullException(nameof(itemName));
            if (!ConfigurationItem.ItemNameIsValid(itemName)) throw new ArgumentException(ResourceStrings.Exception_ConfigurationKeyName, nameof(itemName));
            for (int i = 0; i < _items.Length; ++i)
                if (string.Equals(itemName, _items[i].Name, StringComparison.InvariantCultureIgnoreCase)) return i;
            return -1;
        }

        /// <summary>
        /// Определяет, является ли указанный элемент конфигурации частью текущей секции.
        /// </summary>
        /// <param name="item">Объект <see cref="ConfigurationItem"/>, содержащий данные элемента конфигурации который необходимо проверить.</param>
        /// <returns>Значение true, если <paramref name="item"/> является частью текущей секции; в противном случае — значение false.</returns>
        /// <exception cref="ArgumentNullException">Аргумент <paramref name="item"/> не может быть null.</exception>
        public bool Contains(ConfigurationItem item) => IndexOf(item) >= 0;


        /// <summary>
        /// Определяет, является ли элемент конфигурации с указанным именем частью текущей секции.
        /// </summary>
        /// <param name="itemName">Наименование элемента конфигурации, входимость которого необходимо проверить.</param>
        /// <returns>Значение true, если элемен колекции с наименованием <paramref name="itemName"/> является частью текущей секции; в противном случае — значение false.</returns>
        /// <exception cref="ArgumentNullException">Аргумент <paramref name="itemName"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="itemName"/> содержит недопустимое значение.</exception>
        public bool Contains(string itemName) => FindItem(itemName) != null;

        /// <summary>
        /// Аолучает перечислитель для этой коллекции.
        /// </summary>
        /// <returns>Объект, реализующий интерфейс <see cref="IEnumerator{ConfigurationItem}" />, представляющий перечислитель для этой коллекции.</returns>
        public IEnumerator<ConfigurationItem> GetEnumerator() => new Enumerator<ConfigurationItem>(this);

        #endregion

        #region Внутренние

        protected private abstract void ResetHasChanged();

        protected private abstract void SetHasChanged();

        protected private abstract int ItemIndex(ConfigurationItem item);

        int IConfigurationItem.ItemIndex(ConfigurationItem item) => ItemIndex(item);

        void IConfigurationItem.ResetHasChanged() => ResetHasChanged();

        void IConfigurationItem.SetHasChanged() => SetHasChanged();

        IEnumerator IEnumerable.GetEnumerator() => new Enumerator<ConfigurationItem>(this);

        #endregion

        #endregion

        #region Свойства

        /// <summary>
        /// Получает указатель, указывающий могут ли вноситься изменения в конфигурацию. 
        /// </summary>
        /// <return>Значенеи true, если изменения могут быть внесены; в противном случае — значение false.</return>
        public virtual bool CanEdit => false;

        /// <summary>
        /// Получает количество элементов конфигурации в текущей конфигурации.
        /// </summary>
        public int Count => _items.Length;

        /// <summary>
        /// Получает элемента конфигурации в текущей конфигурации по его индексу.
        /// </summary>
        /// <param name="index">Отсчитываемый он нуля индекс элемента конфигурации в текущей секции.</param>
        /// <returns>Объект <see cref="ConfigurationItem"/>, содержащий данные элемента конфигурации.</returns>
        /// <exception cref="ArgumentOutOfRangeException">Значение аргумента <paramref name="index"/> меньше 0. -или- Значение аргумента <paramref name="index"/> равно или больше значения свойства <see cref="Count"/>.</exception>
        public ConfigurationItem this[int index]
        {
            get {
                if (index < 0 || index >= _items.Length) throw new ArgumentOutOfRangeException(nameof(index), ResourceStrings.Exception_IndexOutOfRange);
                return _items[index];
            }
        }

        /// <summary>
        /// Получает размер данных конфигурации
        /// </summary>
        /// <return>Размер данных конфигурации в байтах</return>
        public virtual int DataSize
        {
            get {
                int retsize = 0;
                foreach (ConfigurationItem item in _items)
                    retsize += item.Size + 4;
                return retsize;
            }
        }

#pragma warning disable CS1591 

        protected virtual int CollectionVersion => 0;

#pragma warning restore CS1591 

        int IEnumerableList<ConfigurationItem>.Version => CollectionVersion;

        #endregion

    }
}
