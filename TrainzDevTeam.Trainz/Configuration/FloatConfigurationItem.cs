﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Globalization;

namespace TrainzDevTeam.Trainz
{

    /// <summary>
    /// Описывает элемент конфигурации содержащий значения с плавающей точкой
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1710")]
    public sealed class FloatConfigurationItem : ConfigurationItem, IReadOnlyList<float>, IEnumerableList<float>
    {

        #region Данные класса

        private float[] _values;
        private int _version = 0;

        #endregion

        #region Конструктор

        internal FloatConfigurationItem(int size, ConfigurationItemType type, string name, int itemPosition, Stream stream) : base(size, type, name, itemPosition, stream)
        {
            var datasize = (size - name.Length - 3);
            if (datasize % 4 != 0) throw new BadFormatException(ResourceStrings.Exception_ErrorConfigurationDataFormat);
            _values = new float[datasize / 4];
            byte[] buffer = new byte[4];
            for (int i = 0; i < _values.Length; ++i) {
                if (stream.Read(buffer, 0, buffer.Length) != buffer.Length) throw new BadFormatException(ResourceStrings.Exception_ErrorConfigurationDataFormat);
                _values[i] = BitConverter.ToSingle(buffer, 0);
            }
        }

        internal FloatConfigurationItem(string name, float[] value) : base(0, ConfigurationItemType.String, name, -1, null) => _values = value;

        #endregion

        #region Методы

        /// <summary>
        /// Устанавливает значение элемента конфигурации.
        /// </summary>
        /// <param name="values">Массив значений для этого элемента конфигурации. Требуется минимум одно значение и не больше 255.</param>
        /// <exception cref="ReadOnlyException">Данные конфигурации доступны только для чтения.</exception>
        /// <exception cref="ArgumentNullException">Аргумент <paramref name="values"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="values"/> должен иметь от 1 до 255 элементов.</exception>
        public void SetValues(params float[] values)
        {
            if (!CanEdit) throw new ReadOnlyException(ResourceStrings.Exception_ConfigurationReadOnly);
            if (values == null) throw new ArgumentNullException(nameof(values));
            if (values.Length == 0 || values.Length > 255) throw new ArgumentException(ResourceStrings.Exception_ConfigurationItemArrayValue, nameof(values));
            _values = values;
            _version++;
            ((IConfigurationItem)this).SetHasChanged();
        }

        /// <summary>
        /// Устанавливает количество значений элемента конфигурации.
        /// </summary>
        /// <param name="count">Количество элементов конфигурации, которое необходимо установить.</param>
        /// <exception cref="ReadOnlyException">Данные конфигурации доступны только для чтения.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Аргумент <paramref name="count"/> должен иметь значение от 1 до 255.</exception>
        public void SetValuesCount(int count)
        {
            if (!CanEdit) throw new ReadOnlyException(ResourceStrings.Exception_ConfigurationReadOnly);
            if (count < 1 || count > 255) throw new ArgumentOutOfRangeException(nameof(count), ResourceStrings.Exception_ConfigurationItemArrayValue);
            if (count != _values.Length) {
                Array.Resize(ref _values, count);
                _version++;
                ((IConfigurationItem)this).SetHasChanged();
            }
        }

        /// <summary>
        /// Возвращает строку, представляющую текущий элемент конфигурации.
        /// </summary>
        /// <returns>Cтрока, представляющая текущий элемент конфигурации.</returns>
        public override string ToString()
        {
            StringBuilder value = new StringBuilder();
            for (int i = 0; i < _values.Length; ++i) {
                if (i > 0) value.Append(", ");
                value.Append(string.Format(CultureInfo.InvariantCulture, "{0:0.0####}", _values[i]));
            }
            return string.Format(CultureInfo.InvariantCulture, "{0} = {1}", Name, value.ToString());
        }

        /// <summary>
        /// Возвращает перечеслитель для значений этого элемента конфигурации.
        /// </summary>
        /// <returns></returns>
        IEnumerator<float> IEnumerable<float>.GetEnumerator() => new Enumerator<float>(this);


        internal override void Save(Stream stream, ProgressEventCallback progressCallBack, ProgressEventArgs eventArgs)
        {
            base.Save(stream, progressCallBack, eventArgs);
            foreach (float value in _values)
                stream.Write(BitConverter.GetBytes(value), 0, 4);
        }

        IEnumerator IEnumerable.GetEnumerator() => new Enumerator<float>(this);

        #endregion

        #region Свойства

        /// <summary>
        /// Получает размер данных текущего элемента конфигурации в байтах.
        /// </summary>
        public override int DataSize => _values.Length * 4;

        /// <summary>
        /// Получает количества значений элемента конфигурации.
        /// </summary>
        public int ValueCount => _values.Length;

        /// <summary>
        /// Получает или задаёт значение элемента конфигурации.
        /// </summary>
        /// <param name="index">Индекс значение элемента конфигурации.</param>
        /// <value>индекс значения элеменка конфигурации от 0 до 255.</value>
        /// <exception cref="ReadOnlyException">Данные конфигурации доступны только для чтения.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Индекс элемента выходит за допустимые пределы от 0 до 255.</exception>
        public float this[int index]
        {
            get {
                if (index < 0 || index > 255) throw new ArgumentOutOfRangeException(nameof(index), ResourceStrings.Exception_ConfigurationItemArrayIndex);
                return index < _values.Length ? _values[index] : 0;
            }
            set {
                if (!CanEdit) throw new ReadOnlyException(ResourceStrings.Exception_ConfigurationReadOnly);
                if (index < 0 || index > 255) throw new ArgumentOutOfRangeException(nameof(index), ResourceStrings.Exception_ConfigurationItemArrayIndex);
                int count = _values.Length;
                if (index >= _values.Length) Array.Resize(ref _values, index + 1);
                if (_values.Length != count || _values[index] != value) {
                    _values[index] = value;
                    _version++;
                    ((IConfigurationItem)this).SetHasChanged();
                }
            }
        }

        int IReadOnlyCollection<float>.Count => ValueCount;

        int IEnumerableList<float>.Count => ValueCount;

        int IEnumerableList<float>.Version => _version;

        #endregion

    }
}
