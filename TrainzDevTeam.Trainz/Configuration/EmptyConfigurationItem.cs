﻿using System.IO;

namespace TrainzDevTeam.Trainz
{
    /// <summary>
    /// Описывает элемент конфигурации, который не имеет значения.
    /// </summary>
    public sealed class EmptyConfigurationItem : ConfigurationItem
    {

        #region Конструктор

        internal EmptyConfigurationItem(int size, ConfigurationItemType type, string name, int itemPosition, Stream stream) : base(size, type, name, itemPosition, stream) { }

        internal EmptyConfigurationItem(string name) : base(0, ConfigurationItemType.String, name, -1, null) { }

        #endregion

        #region Методы

        /// <summary>
        /// Возвращает строку, представляющую текущий элемент конфигурации.
        /// </summary>
        /// <returns>Cтрока, представляющая текущий элемент конфигурации.</returns>
        public override string ToString() => $"{Name} = EMPTY";

        #endregion

        #region Свойства

        /// <summary>
        /// Получает размер данных текущего элемента конфигурации в байтах.
        /// </summary>
        public override int DataSize => SourceSize - Name.Length - 3;

        #endregion

    }
}
