﻿
namespace TrainzDevTeam.Trainz
{
    internal interface IConfigurationItem
    {

        void SetHasChanged();
        void ResetHasChanged();
        int ItemIndex(ConfigurationItem item);
        bool CanEdit { get; }

    }

}
