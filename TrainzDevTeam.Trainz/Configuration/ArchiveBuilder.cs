﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;

namespace TrainzDevTeam.Trainz
{

    /// <summary>
    /// Предоставляет построитьель файла архива cdp на основе частично сохранённых элементов конфигурации
    /// </summary>
    [SuppressMessage("Naming", "CA1710")]
    public sealed class ArchiveBuilder : ReadableConfiguration, IDisposable
    {

        #region Данные класса

        private List<ConfigurationPartialData> _partiallist;
        private bool _disposed = false;

        #endregion

        #region Конструктор

        internal ArchiveBuilder(string[] files)
        {
            var assets = new List<SectionConfigurationItem>();
            var contents = new List<Kuid>();
            var kuidtable = new List<Kuid>();
            var obsolete = new List<Kuid>();
            _partiallist = new List<ConfigurationPartialData>();
            foreach (var filename in files) {
                var part = new ConfigurationPartialData(filename);
                if (part.Item.Type != ConfigurationItemType.Section || !AddEntryToList(assets, (SectionConfigurationItem)part.Item)) throw new InvalidOperationException(ResourceStrings.Exception_RecurringAssetInBuilder);
                var kuiditem = ((SectionConfigurationItem)part.Item).FindItem("kuid") as KuidConfigurationItem;
                if (kuiditem == null) throw new InvalidOperationException(ResourceStrings.Exception_RecurringAssetInBuilder);
                contents.Add(kuiditem.Value);
                var kuidtablesection = ((SectionConfigurationItem)part.Item).FindItem("kuid-table") as SectionConfigurationItem;
                var obsoletetablesection = ((SectionConfigurationItem)part.Item).FindItem("obsolete-table") as SectionConfigurationItem;
                if(kuidtablesection != null) {
                    foreach (KuidConfigurationItem item in kuidtablesection)
                        kuidtable.Add(item.Value);
                }
                if (obsoletetablesection != null) {
                    foreach (KuidConfigurationItem item in obsoletetablesection)
                        obsolete.Add(item.Value);
                }
                _partiallist.Add(part);
            }
            _items = new ConfigurationItem[7];
            _items[0] = new SectionConfigurationItem("assets", assets.ToArray());
            _items[1] = new SectionConfigurationItem("contents-table", KuidListToItems(contents, false));
            _items[2] = new SectionConfigurationItem("kuid-table", KuidListToItems(kuidtable));
            _items[3] = new SectionConfigurationItem("obsolete-table", KuidListToItems(obsolete));
            _items[4] = new StringConfigurationItem("kind", "archive");
            _items[5] = new IntConfigurationItem("package-version", new int[] { 1 });
            _items[6] = new StringConfigurationItem("username", "CDP Builder");
            _items[0].SetParent(this);
            _items[1].SetParent(this);
            _items[2].SetParent(this);
            _items[3].SetParent(this);
            _items[4].SetParent(this);
            _items[5].SetParent(this);
            _items[6].SetParent(this);
        }

        #endregion

        #region Методы

        #region Общие методы

        /// <summary>
        /// Выполняет создание построителя файла архива cdp.
        /// </summary>
        /// <param name="filenames">Список абсолютных или относительных путей к файлам, содержащим сохранённые активы, для построения архива.</param>
        /// <returns>Объект <see cref="ArchiveBuilder"/>, предоставлюящий механизм создания файла архива cdp.</returns>
        public static ArchiveBuilder Create(params string[] filenames)
        {
            if (filenames == null) throw new ArgumentNullException(nameof(filenames));
            if (filenames.Length == 0) throw new ArgumentException(ResourceStrings.Exception_BuilderEmptyPartFiles, nameof(filenames));
            return new ArchiveBuilder(filenames);
        }

        private static bool AddEntryToList(List<SectionConfigurationItem> list, SectionConfigurationItem entry)
        {
            if(!list.Exists(item=> item.Name.Equals(entry.Name, StringComparison.OrdinalIgnoreCase))) {
                list.Add(entry);
                return true;
            }
            return false;
        }

        private static KuidConfigurationItem[] KuidListToItems(List<Kuid> kuids, bool distinct = true)
        {
            var index = 0;
            var query = distinct ? kuids.Distinct() : kuids;
            return query.Select(item => new KuidConfigurationItem(index++.ToString(CultureInfo.InvariantCulture), item)).ToArray();
        }

        #endregion

        #region Вешние

        /// <summary>
        /// Создаёт архив и записывает его в указанный поток
        /// </summary>
        /// <param name="stream">Поток, в который необходимо записать архив</param>
        public void Build(Stream stream)
        {
            if (stream == null) throw new ArgumentNullException(nameof(stream));
            if (!stream.CanWrite) throw new NotSupportedException(ResourceStrings.Exception_CannotWriteStream);
            int datasize = ArchiveSize - 16;
            stream.Write(new byte[] { 65, 67, 83, 36, 1, 0, 0, 0, 0, 0, 0, 0 }, 0, 12);
            stream.Write(BitConverter.GetBytes(datasize), 0, 4);
            var eventargs = new ProgressEventArgs(datasize, 0);
            foreach (ConfigurationItem item in _items)
                ConfigurationInternalFunctions.SaveConfigurationItem(item, stream, null, eventargs);
        }

        public void Dispose()
        {
            if (!_disposed) {
                foreach (var part in _partiallist) part.Dispose();
                _disposed = true;
            }
        }

        #endregion

        #region Внутренние

        private protected override int ItemIndex(ConfigurationItem item)
        {
            for (var i = 0; i < _items.Length; ++i)
                if (_items[i] == item) return i;
            return -1;
        }

        private protected override void ResetHasChanged() { }

        private protected override void SetHasChanged() { }

        #endregion

        #endregion

        #region Свойства

        /// <summary>
        /// Возвращает размер архива
        /// </summary>
        public int ArchiveSize
        {
            get {
                int retsize = 16;
                foreach (ConfigurationItem item in _items)
                    retsize += item.Size + 4;
                return retsize;
            }
        }

        #endregion

    }
}
