﻿
namespace TrainzDevTeam.Trainz
{
    /// <summary>
    /// Определяет типы элементов конфигурации.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1720")]
    public enum ConfigurationItemType
    {
        
        /// <summary>
        /// Секция содержащая другие блоки.
        /// </summary>
        Section = 0x00,

        /// <summary>
        /// Целочисленое значение или их группа.
        /// </summary>
        Int = 0x01,

        /// <summary>
        /// Значение с плавающей точкой или их группа.
        /// </summary>
        Float = 0x02,

        /// <summary>
        /// Строковое значение.
        /// </summary>
        String = 0x03,

        /// <summary>
        /// Данные содержащие файл.
        /// </summary>
        FileData = 0x04,

        /// <summary>
        /// Секция не содержащая данных.
        /// </summary>
        Empty = 0x05,

        /// <summary>
        /// Запакованный номер kuid.
        /// </summary>
        Kuid = 0x0d,

    }
}
