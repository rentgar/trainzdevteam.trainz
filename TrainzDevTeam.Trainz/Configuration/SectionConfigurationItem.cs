﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Globalization;

namespace TrainzDevTeam.Trainz
{
    /// <summary>
    /// Описывает элемент конфигурации содержащий другие элементы конфигурации.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1710")]
    public sealed class SectionConfigurationItem : ConfigurationItem, IConfigurationItemCollection, IConfigurationItemReadOnlyCollection, IReadOnlyList<ConfigurationItem>, IEnumerableList<ConfigurationItem>
    {

        #region Данные класса

        private ConfigurationItem[] _items;
        private int _version = 0;

        #endregion

        #region Конструктор

        internal SectionConfigurationItem(int size, ConfigurationItemType type, string name, int itemPosition, Stream stream, ConfigurationItem[] items) : base(size, type, name, itemPosition, stream)
        {
            _items = items;
            foreach (ConfigurationItem item in _items)
                item.SetParent(this);
        }

        internal SectionConfigurationItem(string name) : base(0, ConfigurationItemType.String, name, -1, null) => _items = Array.Empty<ConfigurationItem>();

        internal SectionConfigurationItem(string name, ConfigurationItem[] items) : base(0, ConfigurationItemType.String, name, -1, null) => _items = items;

        #endregion

        #region Методы

        #region Внешние

        /// <summary>
        /// Выполняет поиск элемента конфигурации в коллекции.
        /// </summary>
        /// <param name="itemName">Имя элемента конфигурации, который необходимо найти.</param>
        /// <returns>Объект <see cref="ConfigurationItem"/>, содержащий данные найденого элемента конфигурации; или значение null, если элемент не был найден.</returns>
        /// <exception cref="ArgumentNullException">Аргумент <paramref name="itemName"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="itemName"/> содержит недопустимое значение.</exception>
        public ConfigurationItem FindItem(string itemName)
        {
            if (itemName == null) throw new ArgumentNullException(nameof(itemName));
            if (!ItemNameIsValid(itemName)) throw new ArgumentException(ResourceStrings.Exception_ConfigurationKeyName, nameof(itemName));
            return _items.FirstOrDefault(item => item.Name.Equals(itemName, StringComparison.InvariantCultureIgnoreCase));
        }

        /// <summary>
        /// Добавляет новую секцию в коллекцию.
        /// </summary>
        /// <param name="itemName">Наименование добавляемого эемента конфигурации.</param>
        /// <returns>Объект <see cref="SectionConfigurationItem"/>, представляющий новый элемент конфигурации.</returns>
        /// <exception cref="ReadOnlyException">Данные конфигурации доступны только для чтения.</exception>
        /// <exception cref="ArgumentNullException">Аргумент <paramref name="itemName"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="itemName"/> содержит недопустимое значение.</exception>
        /// <exception cref="AlreadyExistException">Другой элемент конфигурации с наименованием <paramref name="itemName"/> уже присутствует в коллекции.</exception>
        public SectionConfigurationItem AddItem(string itemName)
        {
            if (!CanEdit) throw new ReadOnlyException(ResourceStrings.Exception_ConfigurationReadOnly);
            if (itemName == null) throw new ArgumentNullException(nameof(itemName));
            if (!ItemNameIsValid(itemName)) throw new ArgumentException(ResourceStrings.Exception_ConfigurationKeyName, nameof(itemName));
            if (Contains(itemName)) throw new AlreadyExistException(string.Format(CultureInfo.CurrentCulture, ResourceStrings.Exception_ConfigurationItemAlreadyExist, itemName));
            var newitem = new SectionConfigurationItem(itemName);
            Array.Resize(ref _items, _items.Length + 1);
            _items[_items.Length - 1] = newitem;
            newitem.SetParent(this);
            ((IConfigurationItem)this).SetHasChanged();
            _version++;
            return newitem;
        }

        /// <summary>
        /// Добавляет новый элемент конфигурации содержащий целочисленные данные.
        /// </summary>
        /// <param name="itemName">Наименование добавляемого эемента конфигурации.</param>
        /// <param name="values">Значения элемента конфигурации в количестве от 1 до 255.</param>
        /// <returns>Объект <see cref="IntConfigurationItem"/>, представляющий новый элемент конфигурации.</returns>
        /// <exception cref="ReadOnlyException">Данные конфигурации доступны только для чтения.</exception>
        /// <exception cref="ArgumentNullException">Аргумент <paramref name="itemName"/> не может быть null. -или- Аргумент <paramref name="values"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="itemName"/> содержит недопустимое значение. -или- Аргумент <paramref name="values"/> должен иметь от 1 до 255 элементов.</exception>
        /// <exception cref="AlreadyExistException">Другой элемент конфигурации с наименованием <paramref name="itemName"/> уже присутствует в коллекции.</exception>
        public IntConfigurationItem AddItem(string itemName, params int[] values)
        {
            if (!CanEdit) throw new ReadOnlyException(ResourceStrings.Exception_ConfigurationReadOnly);
            if (itemName == null) throw new ArgumentNullException(nameof(itemName));
            if (!ItemNameIsValid(itemName)) throw new ArgumentException(ResourceStrings.Exception_ConfigurationKeyName, nameof(itemName));
            if (values == null) throw new ArgumentNullException(nameof(values));
            if (values.Length < 1 || values.Length > 255) throw new ArgumentException(ResourceStrings.Exception_ConfigurationItemArrayValue, nameof(values));
            if (Contains(itemName)) throw new AlreadyExistException(string.Format(CultureInfo.CurrentCulture, ResourceStrings.Exception_ConfigurationItemAlreadyExist, itemName));
            var newitem = new IntConfigurationItem(itemName, values);
            Array.Resize(ref _items, _items.Length + 1);
            _items[_items.Length - 1] = newitem;
            newitem.SetParent(this);
            ((IConfigurationItem)this).SetHasChanged();
            _version++;
            return newitem;
        }

        /// <summary>
        /// Добавляет новый элемент конфигурации содержащий значения с плавающей точкой.
        /// </summary>
        /// <param name="itemName">Наименование добавляемого эемента конфигурации.</param>
        /// <param name="values">Значения элемента конфигурации в количестве от 1 до 255.</param>
        /// <returns>Объект <see cref="FloatConfigurationItem"/>, представляющий новый элемент конфигурации.</returns>
        /// <exception cref="ReadOnlyException">Данные конфигурации доступны только для чтения.</exception>
        /// <exception cref="ArgumentNullException">Аргумент <paramref name="itemName"/> не может быть null. -или- Аргумент <paramref name="values"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="itemName"/> содержит недопустимое значение. -или- Аргумент <paramref name="values"/> должен иметь от 1 до 255 элементов.</exception>
        /// <exception cref="AlreadyExistException">Другой элемент конфигурации с наименованием <paramref name="itemName"/> уже присутствует в коллекции.</exception>
        public FloatConfigurationItem AddItem(string itemName, params float[] values)
        {
            if (!CanEdit) throw new ReadOnlyException(ResourceStrings.Exception_ConfigurationReadOnly);
            if (itemName == null) throw new ArgumentNullException(nameof(itemName));
            if (!ItemNameIsValid(itemName)) throw new ArgumentException(ResourceStrings.Exception_ConfigurationKeyName, nameof(itemName));
            if (values == null) throw new ArgumentNullException(nameof(values));
            if (values.Length < 1 || values.Length > 255) throw new ArgumentException(ResourceStrings.Exception_ConfigurationItemArrayValue, nameof(values));
            if (Contains(itemName)) throw new AlreadyExistException(string.Format(CultureInfo.CurrentCulture, ResourceStrings.Exception_ConfigurationItemAlreadyExist, itemName));
            var newitem = new FloatConfigurationItem(itemName, values);
            Array.Resize(ref _items, _items.Length + 1);
            _items[_items.Length - 1] = newitem;
            newitem.SetParent(this);
            ((IConfigurationItem)this).SetHasChanged();
            _version++;
            return newitem;
        }

        /// <summary>
        /// Добавляет новый элемент конфигурации содержащий строкове значение.
        /// </summary>
        /// <param name="itemName">Наименование добавляемого эемента конфигурации.</param>
        /// <param name="value">Строка, которая будет являться значением элемента конфигурации.</param>
        /// <returns>Объект <see cref="StringConfigurationItem"/>, представляющий новый элемент конфигурации.</returns>
        /// <exception cref="ReadOnlyException">Данные конфигурации доступны только для чтения.</exception>
        /// <exception cref="ArgumentNullException">Аргумент <paramref name="itemName"/> не может быть null. -или- Аргумент <paramref name="value"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="itemName"/> содержит недопустимое значение.</exception>
        /// <exception cref="AlreadyExistException">Другой элемент конфигурации с наименованием <paramref name="itemName"/> уже присутствует в коллекции.</exception>
        public StringConfigurationItem AddItem(string itemName, string value)
        {
            if (!CanEdit) throw new ReadOnlyException(ResourceStrings.Exception_ConfigurationReadOnly);
            if (itemName == null) throw new ArgumentNullException(nameof(itemName));
            if (!ItemNameIsValid(itemName)) throw new ArgumentException(ResourceStrings.Exception_ConfigurationKeyName, nameof(itemName));
            if (value == null) throw new ArgumentNullException(nameof(value));
            if (Contains(itemName)) throw new AlreadyExistException(string.Format(CultureInfo.CurrentCulture, ResourceStrings.Exception_ConfigurationItemAlreadyExist, itemName));
            var newitem = new StringConfigurationItem(itemName, value);
            Array.Resize(ref _items, _items.Length + 1);
            _items[_items.Length - 1] = newitem;
            newitem.SetParent(this);
            ((IConfigurationItem)this).SetHasChanged();
            _version++;
            return newitem;
        }

        /// <summary>
        /// Добавляет новый элемент конфигурации содержащий значение типа KUID.
        /// </summary>
        /// <param name="itemName">Наименование добавляемого эемента конфигурации.</param>
        /// <param name="value">Экземпляр структуры <see cref="Kuid"/>, содержащий KUID номер для элемента конфигурации.</param>
        /// <returns>Объект <see cref="KuidConfigurationItem"/>, представляющий новый элемент конфигурации.</returns>
        /// <exception cref="ReadOnlyException">Данные конфигурации доступны только для чтения.</exception>
        /// <exception cref="ArgumentNullException">Аргумент <paramref name="itemName"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="itemName"/> содержит недопустимое значение.</exception>
        /// <exception cref="AlreadyExistException">Другой элемент конфигурации с наименованием <paramref name="itemName"/> уже присутствует в коллекции.</exception>
        public KuidConfigurationItem AddItem(string itemName, Kuid value)
        {
            if (!CanEdit) throw new ReadOnlyException(ResourceStrings.Exception_ConfigurationReadOnly);
            if (itemName == null) throw new ArgumentNullException(nameof(itemName));
            if (!ItemNameIsValid(itemName)) throw new ArgumentException(ResourceStrings.Exception_ConfigurationKeyName, nameof(itemName));
            if (Contains(itemName)) throw new AlreadyExistException(string.Format(CultureInfo.CurrentCulture, ResourceStrings.Exception_ConfigurationItemAlreadyExist, itemName));
            var newitem = new KuidConfigurationItem(itemName, value);
            Array.Resize(ref _items, _items.Length + 1);
            _items[_items.Length - 1] = newitem;
            newitem.SetParent(this);
            ((IConfigurationItem)this).SetHasChanged();
            _version++;
            return newitem;
        }

        /// <summary>
        /// Добавляет новый пустой элемент конфигурации.
        /// </summary>
        /// <param name="itemName">Наименование добавляемого эемента конфигурации.</param>
        /// <returns>Объект <see cref="EmptyConfigurationItem"/>, представляющий новый элемент конфигурации.</returns>
        /// <exception cref="ReadOnlyException">Данные конфигурации доступны только для чтения.</exception>
        /// <exception cref="ArgumentNullException">Аргумент <paramref name="itemName"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="itemName"/> содержит недопустимое значение.</exception>
        /// <exception cref="AlreadyExistException">Другой элемент конфигурации с наименованием <paramref name="itemName"/> уже присутствует в коллекции.</exception>
        public EmptyConfigurationItem AddEmptyItem(string itemName)
        {
            if (!CanEdit) throw new ReadOnlyException(ResourceStrings.Exception_ConfigurationReadOnly);
            if (itemName == null) throw new ArgumentNullException(nameof(itemName));
            if (!ItemNameIsValid(itemName)) throw new ArgumentException(ResourceStrings.Exception_ConfigurationKeyName, nameof(itemName));
            if (Contains(itemName)) throw new AlreadyExistException(string.Format(CultureInfo.CurrentCulture, ResourceStrings.Exception_ConfigurationItemAlreadyExist, itemName));
            var newitem = new EmptyConfigurationItem(itemName);
            Array.Resize(ref _items, _items.Length + 1);
            _items[_items.Length - 1] = newitem;
            newitem.SetParent(this);
            ((IConfigurationItem)this).SetHasChanged();
            _version++;
            return newitem;
        }

        /// <summary>
        /// Выполняет поиск индекса для входящего элемента конфигурации.
        /// </summary>
        /// <param name="item">Объект <see cref="ConfigurationItem"/>, содержащий данные элемента конфигурации индекс которого необходимо найти.</param>
        /// <returns>Отсчитываемый от нуля индекс наденого элемента конфигурации, или значение -1, если жлемент не был найден в коллекции.</returns>
        /// <exception cref="ArgumentNullException">Аргумент <paramref name="item"/> не может быть null.</exception>
        public int IndexOf(ConfigurationItem item)
        {
            if (item == null) throw new ArgumentNullException(nameof(item));
            return ItemIndex(item);
        }

        /// <summary>
        /// Выполняет поиск индекса для входящего элемента конфигурации.
        /// </summary>
        /// <param name="itemName">Наименование тега, который необходимо найти.</param>
        /// <returns>Индекс элемента конфигурации или -1 - если жлемент не был найден.</returns>
        /// <exception cref="ArgumentNullException">Аргумент <paramref name="itemName"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="itemName"/> содержит недопустимое значение.</exception>
        public int IndexOf(string itemName)
        {
            if (itemName == null) throw new ArgumentNullException(nameof(itemName));
            if (!ItemNameIsValid(itemName)) throw new ArgumentException(ResourceStrings.Exception_ConfigurationKeyName, nameof(itemName));
            for (int i = 0; i < _items.Length; ++i)
                if (string.Equals(itemName, _items[i].Name, StringComparison.InvariantCultureIgnoreCase)) return i;
            return -1;
        }

        /// <summary>
        /// Определяет, является ли указанный элемент конфигурации частью текущей секции.
        /// </summary>
        /// <param name="item">Объект <see cref="ConfigurationItem"/>, содержащий данные элемента конфигурации который необходимо проверить.</param>
        /// <returns>Значение true, если <paramref name="item"/> является частью текущей секции; в противном случае — значение false.</returns>
        /// <exception cref="ArgumentNullException">Аргумент <paramref name="item"/> не может быть null.</exception>
        public bool Contains(ConfigurationItem item) => IndexOf(item) >= 0;


        /// <summary>
        /// Определяет, является ли элемент конфигурации с указанным именем частью текущей секции.
        /// </summary>
        /// <param name="itemName">Наименование элемента конфигурации, входимость которого необходимо проверить.</param>
        /// <returns>Значение true, если элемен колекции с наименованием <paramref name="itemName"/> является частью текущей секции; в противном случае — значение false.</returns>
        /// <exception cref="ArgumentNullException">Аргумент <paramref name="itemName"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="itemName"/> содержит недопустимое значение.</exception>
        public bool Contains(string itemName) => FindItem(itemName) != null;

        /// <summary>
        /// Выполняет удаление элемента конфигурации из текущей секции.
        /// </summary>
        /// <param name="item">Объект <see cref="ConfigurationItem"/>, представляющий удаляемый элемента конфигурации.</param>
        /// <returns>Значение true, если элемент конфигурации был удачно удалён; в противном случае — значение false.</returns>
        /// <exception cref="ReadOnlyException">Данные конфигурации доступны только для чтения.</exception>
        /// <exception cref="ArgumentNullException">Аргумент <paramref name="item"/> не может быть null.</exception>
        public bool RemoveItem(ConfigurationItem item)
        {
            var index = IndexOf(item);
            if (index >= 0) RemoveItem(index);
            return index >= 0;
        }

        /// <summary>
        /// Выполняет удаление элемента конфигурации из текущей секции.
        /// </summary>
        /// <param name="itemName">Наименование элемента конфигурации, который необходимо удалить.</param>
        /// <returns>Значение true, если элемент конфигурации был удачно удалён; в противном случае — значение false.</returns>
        /// <exception cref="ReadOnlyException">Данные конфигурации доступны только для чтения.</exception>
        /// <exception cref="ArgumentNullException">Аргумент <paramref name="itemName"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="itemName"/> содержит недопустимое значение.</exception>
        public bool RemoveItem(string itemName)
        {
            var index = IndexOf(itemName);
            if (index >= 0) RemoveItem(index);
            return index >= 0;
        }

        /// <summary>
        /// Выполняет удаление элемента конфигурации из текущей секции.
        /// </summary>
        /// <param name="index">Отсчитываемый он нуля индекс удаляемого элемента конфигурации в текущей секции.</param>
        /// <exception cref="ReadOnlyException">Данные конфигурации доступны только для чтения.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Значение аргумента <paramref name="index"/> меньше 0. -или- Значение аргумента <paramref name="index"/> равно или больше значения свойства <see cref="Count"/>.</exception>
        public void RemoveItem(int index)
        {
            if (!CanEdit) throw new ReadOnlyException(ResourceStrings.Exception_ConfigurationReadOnly);
            if (index < 0 || index >= _items.Length) throw new ArgumentOutOfRangeException(nameof(index), ResourceStrings.Exception_IndexOutOfRange);
            _items[index].SetParent(null);
            _items[index].SetStream(null);
            if (index < _items.Length - 1) Array.Copy(_items, index + 1, _items, index, _items.Length - index - 1);
            Array.Resize(ref _items, _items.Length - 1);
            ((IConfigurationItem)this).SetHasChanged();
            _version++;
        }

        /// <summary>
        /// Аолучает перечислитель для этой коллекции.
        /// </summary>
        /// <returns>Объект, реализующий интерфейс <see cref="IEnumerator{ConfigurationItem}" />, представляющий перечислитель для этой коллекции.</returns>
        public IEnumerator<ConfigurationItem> GetEnumerator() => new Enumerator<ConfigurationItem>(this);

        /// <summary>
        /// Возвращает строку, представляющую текущий элемент конфигурации.
        /// </summary>
        /// <returns>Cтрока, представляющая текущий элемент конфигурации.</returns>
        public override string ToString() => Name;

        #endregion

        #region Внутренние

        private protected override int ItemIndex(ConfigurationItem item)
        {
            for (int i = 0; i < _items.Length; ++i)
                if (_items[i] == item) return i;
            return -1;
        }

        IEnumerator IEnumerable.GetEnumerator() => new Enumerator<ConfigurationItem>(this);

        internal override void Save(Stream stream, ProgressEventCallback progressCallBack, ProgressEventArgs eventArgs)
        {
            base.Save(stream, progressCallBack, eventArgs);
            int position = (int)stream.Position;
            foreach (ConfigurationItem item in _items) {
                int blocksize = item.Size;
                int nextitemposition = (int)stream.Position + blocksize + 4;
                if (item.CanSave && (item.SourceSize == 0 || item.HasChanged)) {
                    stream.Write(BitConverter.GetBytes(blocksize), 0, 4);
                    item.Save(stream, progressCallBack, eventArgs);
                    if (item.Type == ConfigurationItemType.Section) position = (int)stream.Position;
                }
                stream.Seek(nextitemposition, SeekOrigin.Begin);
                if (position < stream.Position) eventArgs.AddProgress(stream.Position - position);
                position = (int)stream.Position;
                progressCallBack?.Invoke(eventArgs);
            }
        }

        internal void SaveOnlyHeader(Stream stream, ProgressEventCallback progressCallBack, ProgressEventArgs eventArgs) => base.Save(stream, progressCallBack, eventArgs);

        private protected override void ResetHasChanged()
        {
            foreach (IConfigurationItem item in _items)
                item.ResetHasChanged();
            base.ResetHasChanged();
        }

        internal override void SetStream(Stream stream)
        {
            foreach (ConfigurationItem item in _items)
                item.SetStream(stream);
            base.SetStream(stream);
        }

        internal override void SetItemPosition(int newPosition, Stream stream)
        {
            base.SetItemPosition(newPosition, stream);
            var nextposition = newPosition + Name.Length + 3 + 4;
            foreach (ConfigurationItem item in _items)
            {
                item.SetItemPosition(nextposition, stream);
                nextposition += item.Size + 4;

            }
        }

        #endregion

        #endregion

        #region Свойства

        /// <summary>
        /// Получает размер данных текущего элемента конфигурации в байтах.
        /// </summary>
        public override int DataSize
        {
            get
            {
                int fullsize = 0;
                foreach (ConfigurationItem item in _items)
                    fullsize += item.Size + 4;
                return fullsize;
            }
        }

        /// <summary>
        /// Получает количество элементов конфигурации в текущей секции.
        /// </summary>
        public int Count => _items.Length;

        /// <summary>
        /// Получает элемента конфигурации в текущей секции по его индексу.
        /// </summary>
        /// <param name="index">Отсчитываемый он нуля индекс элемента конфигурации в текущей секции.</param>
        /// <returns>Объект <see cref="ConfigurationItem"/>, содержащий данные элемента конфигурации.</returns>
        /// <exception cref="ArgumentOutOfRangeException">Значение аргумента <paramref name="index"/> меньше 0. -или- Значение аргумента <paramref name="index"/> равно или больше значения свойства <see cref="Count"/>.</exception>
        public ConfigurationItem this[int index]
        {
            get {
                if (index < 0 || index >= _items.Length) throw new ArgumentOutOfRangeException(nameof(index), ResourceStrings.Exception_IndexOutOfRange);
                return _items[index];
            }
        }

        int IEnumerableList<ConfigurationItem>.Version => _version;

        #endregion

    }
}
