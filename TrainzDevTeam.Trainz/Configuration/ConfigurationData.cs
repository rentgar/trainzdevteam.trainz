﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

namespace TrainzDevTeam.Trainz
{
    
    /// <summary>
    /// Описывает данные конфигурации.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1710")]
    public sealed class ConfigurationData : EditableConfiguration, IDisposable
    {

        #region Данные класса

        private int _startposition;
        private byte[] _header;
        private bool _readonly;
        private bool _disposed = false;

        #endregion

        #region Конструктор

        internal ConfigurationData(Stream stream, bool readOnly, ProgressEventCallback progressCallback)
        {
            _startposition = (int)stream.Position;
            try {
                if (Encoding.ASCII.GetString(stream.ReadBytes(4)) != "ACS$") throw new BadFormatException(ResourceStrings.Exception_BadConfigurationDataFormat);
                _header = stream.ReadBytes(8);
                var dataposition = (int)stream.Position;
                int datasize = stream.ReadInt32();
                if (dataposition + datasize + 4 > stream.Length) throw new BadFormatException(ResourceStrings.Exception_ErrorConfigurationDataFormat);
                _items = ConfigurationInternalFunctions.ParseConfigurationDataBlock(stream, dataposition, datasize, progressCallback, new ProgressEventArgs(datasize, 0));
                foreach (ConfigurationItem item in _items) item.SetParent(this);
            } catch (Exception ex) { throw new BadFormatException(ResourceStrings.Exception_BadConfigurationDataFormat, ex); }
            Stream = stream;
            _readonly = readOnly;
        }

        #endregion

        #region Методы

        #region Статические

        /// <summary>
        /// Создаёт новую конфигурациию в файле.
        /// </summary>
        /// <param name="fileName">Абсолютный или относительный путь к файлу.</param>
        /// <returns>Объект <see cref="ConfigurationData"/>, содержащий данные конфигурации.</returns>
        /// <exception cref="ArgumentNullException">Аргумент <paramref name="fileName"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="fileName"/> представляет собой пустую строку (""), содержащую только пробел или хотя бы один недопустимый символ. -или- path ссылается на устройство нефайлового типа, например "con:", "com1:", "lpt1:" и т. д., в среде NTFS.</exception>
        /// <exception cref="NotSupportedException">Аргумент <paramref name="fileName"/> ссылается на устройство нефайлового типа, например "con:", "com1:", "lpt1:" и т. д., в среде, отличной от NTFS.</exception>
        /// <exception cref="FileNotFoundException">Не удается найти файл, заданный аргументом <paramref name="fileName"/>.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        /// <exception cref="System.Security.SecurityException"> У вызывающего объекта отсутствует необходимое разрешение.</exception>
        /// <exception cref="DirectoryNotFoundException">Указан недопустимый путь (например, ведущий на несопоставленный диск).</exception>
        /// <exception cref="PathTooLongException">Указанный путь, имя файла или оба значения превышают максимальную длину, заданную в системе. Например, для платформ на основе Windows длина пути должна составлять менее 248 знаков, а длина имен файлов — менее 260 знаков.</exception>
        /// <exception cref="UnauthorizedAccessException">Файл или каталог доступен только для чтения.</exception>
        public static ConfigurationData CreateFile(string fileName)
        {
            Stream stream = new FileStream(fileName, FileMode.Create, FileAccess.ReadWrite, FileShare.Read, 4194304);
            stream.Write(new byte[] { 41, 43, 53, 24, 01, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, 0, 16);
            stream.Seek(0, SeekOrigin.Begin);
            return new ConfigurationData(stream, false, null);
        }

        /// <summary>
        /// Открывает данные конфигурации из файла (Например, файл распространяемых дополнений .cdp).
        /// </summary>
        /// <param name="fileName">Абсолютный или относительный путь к файлу.</param>
        /// <param name="readOnly">Указатель, что данные будут доступны только для чтения.</param>
        /// <param name="progressCallback">Делегат <see cref="ProgressEventCallback"/>, для обратного вызова сообщений о ходе процесса разбора данных.</param>
        /// <returns>Объект <see cref="ConfigurationData"/>, содержащий данные конфигурации.</returns>
        /// <exception cref="ArgumentNullException">Аргумент <paramref name="fileName"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="fileName"/> представляет собой пустую строку (""), содержащую только пробел или хотя бы один недопустимый символ. -или- path ссылается на устройство нефайлового типа, например "con:", "com1:", "lpt1:" и т. д., в среде NTFS.</exception>
        /// <exception cref="NotSupportedException">Аргумент <paramref name="fileName"/> ссылается на устройство нефайлового типа, например "con:", "com1:", "lpt1:" и т. д., в среде, отличной от NTFS.</exception>
        /// <exception cref="FileNotFoundException">Не удается найти файл, заданный аргументом <paramref name="fileName"/>.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        /// <exception cref="System.Security.SecurityException"> У вызывающего объекта отсутствует необходимое разрешение.</exception>
        /// <exception cref="DirectoryNotFoundException">Указан недопустимый путь (например, ведущий на несопоставленный диск).</exception>
        /// <exception cref="PathTooLongException">Указанный путь, имя файла или оба значения превышают максимальную длину, заданную в системе. Например, для платформ на основе Windows длина пути должна составлять менее 248 знаков, а длина имен файлов — менее 260 знаков.</exception>
        /// <exception cref="UnauthorizedAccessException">Файл или каталог доступен только для чтения</exception>
        /// <exception cref="BadFormatException">Файл <paramref name="fileName"/> не является действительным файлом конфигурации.</exception>
        public static ConfigurationData OpenFromFile(string fileName, bool readOnly, ProgressEventCallback progressCallback)
        {
            Stream stream = new FileStream(fileName, FileMode.Open, readOnly ? FileAccess.Read : FileAccess.ReadWrite, FileShare.Read, 4194304);
            return new ConfigurationData(stream, readOnly, progressCallback);
        }

        /// <summary>
        /// Открывает данные конфигурации из файла (Например, файл распространяемых дополнений .cdp).
        /// </summary>
        /// <param name="fileName">Абсолютный или относительный путь к файлу.</param>
        /// <param name="readOnly">Указатель, что данные будут доступны только для чтения.</param>
        /// <returns>Объект <see cref="ConfigurationData"/>, содержащий данные конфигурации.</returns>
        /// <exception cref="ArgumentNullException">Аргумент <paramref name="fileName"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="fileName"/> представляет собой пустую строку (""), содержащую только пробел или хотя бы один недопустимый символ. -или- path ссылается на устройство нефайлового типа, например "con:", "com1:", "lpt1:" и т. д., в среде NTFS.</exception>
        /// <exception cref="NotSupportedException">Аргумент <paramref name="fileName"/> ссылается на устройство нефайлового типа, например "con:", "com1:", "lpt1:" и т. д., в среде, отличной от NTFS.</exception>
        /// <exception cref="FileNotFoundException">Не удается найти файл, заданный аргументом <paramref name="fileName"/>.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        /// <exception cref="System.Security.SecurityException"> У вызывающего объекта отсутствует необходимое разрешение.</exception>
        /// <exception cref="DirectoryNotFoundException">Указан недопустимый путь (например, ведущий на несопоставленный диск).</exception>
        /// <exception cref="PathTooLongException">Указанный путь, имя файла или оба значения превышают максимальную длину, заданную в системе. Например, для платформ на основе Windows длина пути должна составлять менее 248 знаков, а длина имен файлов — менее 260 знаков.</exception>
        /// <exception cref="BadFormatException">Файл <paramref name="fileName"/> не является действительным файлом конфигурации.</exception>
        public static ConfigurationData OpenFromFile(string fileName, bool readOnly) => OpenFromFile(fileName, readOnly, null);

        /// <summary>
        /// Открывает данные конфигурации из файла (Например, файл распространяемых дополнений .cdp).
        /// </summary>
        /// <param name="fileName">Абсолютный или относительный путь к файлу.</param>
        /// <returns>Объект <see cref="ConfigurationData"/>, содержащий данные конфигурации.</returns>
        /// <exception cref="ArgumentNullException">Аргумент <paramref name="fileName"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="fileName"/> представляет собой пустую строку (""), содержащую только пробел или хотя бы один недопустимый символ. -или- path ссылается на устройство нефайлового типа, например "con:", "com1:", "lpt1:" и т. д., в среде NTFS.</exception>
        /// <exception cref="NotSupportedException">Аргумент <paramref name="fileName"/> ссылается на устройство нефайлового типа, например "con:", "com1:", "lpt1:" и т. д., в среде, отличной от NTFS.</exception>
        /// <exception cref="FileNotFoundException">Не удается найти файл, заданный аргументом <paramref name="fileName"/>.</exception>
        /// <exception cref="IOException">Ошибка ввода-вывода.</exception>
        /// <exception cref="System.Security.SecurityException"> У вызывающего объекта отсутствует необходимое разрешение.</exception>
        /// <exception cref="DirectoryNotFoundException">Указан недопустимый путь (например, ведущий на несопоставленный диск).</exception>
        /// <exception cref="PathTooLongException">Указанный путь, имя файла или оба значения превышают максимальную длину, заданную в системе. Например, для платформ на основе Windows длина пути должна составлять менее 248 знаков, а длина имен файлов — менее 260 знаков.</exception>
        /// <exception cref="BadFormatException">Файл <paramref name="fileName"/> не является действительным файлом конфигурации.</exception>
        public static ConfigurationData OpenFromFile(string fileName) => OpenFromFile(fileName, false, null);

        #endregion

        #region Внешние

        /// <summary>
        /// Выполняет сохранение внесённых изменений
        /// </summary>
        /// <param name="progressCallback">Делегат <see cref="ProgressEventCallback"/>, для обратного вызова сообщений о ходе процесса сохранения данных.</param>
        public void Save(ProgressEventCallback progressCallback)
        {
            if (_readonly) throw new ReadOnlyException(ResourceStrings.Exception_ConfigurationReadOnly);
            var newfilesize = DataSize + 16;
            if (Stream.Length < newfilesize) Stream.SetLength(newfilesize);
            Stream.Seek(12, SeekOrigin.Begin);
            DataMoveItem[] movedateitems = CreateMoveDataList(_items, Stream.ReadInt32(), 0, Stream);
            ProgressEventArgs eventargs = null;
            var moveddatasize = 0;
            if (movedateitems.Length > 0) {
                //Array.Sort(movedateitems);
                for (var i = 0; i < movedateitems.Length - 1; ++i) {
                    var j = i > 0 ? i - 1 : i + 1;
                    while(j< movedateitems.Length) {
                        if(i!= j) {
                            var resul = movedateitems[i].CompareTo(movedateitems[j]);
                            if ((j > i && resul > 0) || (j < i && resul < 0)) {
                                var temp = movedateitems[i];
                                movedateitems[i] = movedateitems[j];
                                movedateitems[j] = temp;
                            }
                        }
                        ++j;
                    }
                }

                foreach (DataMoveItem moveditem in movedateitems) moveddatasize += moveditem.Size;
                eventargs = new ProgressEventArgs(moveddatasize + newfilesize - 16, 0);
                progressCallback?.Invoke(eventargs);
                foreach (DataMoveItem moveditem in movedateitems) {
                    DataMoveItem.MoveData(Stream, moveditem);
                    eventargs.AddProgress(moveditem.Size);
                    progressCallback?.Invoke(eventargs);
                }
            }
            if (eventargs == null) eventargs = new ProgressEventArgs(newfilesize - 16, 0);
            Stream.Seek(12, SeekOrigin.Begin);
            Stream.Write(BitConverter.GetBytes(newfilesize - 16), 0, 4);
            SaveSection(Stream, progressCallback, eventargs);
            Stream.SetLength(newfilesize);
            ResetHasChanged();
        }

        /// <summary>
        /// Освобождает все ресурсы, занятые <see cref="ConfigurationData"/>
        /// </summary>
        public void Dispose()
        {
            if (!_disposed) {
                Stream.Dispose();
                _disposed = true;
            }
        }

        #endregion

        #region Внутренние

        private DataMoveItem[] CreateMoveDataList(IReadOnlyList<ConfigurationItem> list, int dataSize, int moveVector, Stream stream)
        {
            var retlist = new List<DataMoveItem>();
            var index = 0;
            while(index < list.Count) {
                var item = list[index];
                if (stream.Position > dataSize + 16) break;
                var blockposition = (int)stream.Position;
                var blocksize = stream.ReadInt32();
                var blockname = ConfigurationInternalFunctions.ReadConfigurationDataItemName(stream);
                var blocktype = (ConfigurationItemType)stream.ReadByte();
                if(item.SourceName == blockname) { //Если обновление существующего элемента
                    if (item.HasChanged) {      //Если элемент имеет изменения
                        if(blocktype == ConfigurationItemType.Section && item.Type == ConfigurationItemType.Section) { //Если элемент является блоком
                            var movedataitems = CreateMoveDataList((SectionConfigurationItem)item, dataSize, moveVector, stream);
                            if(moveVector != 0 && (retlist.Count == 0 || retlist[retlist.Count-1].MoveVector != moveVector)) {
                                var newmovedata = new DataMoveItem(blockposition, blockname.Length + 7, moveVector);
                                retlist.Add(newmovedata);
                            } else if (moveVector != 0) retlist[retlist.Count - 1].AddSize(blockname.Length + 7);
                            if(moveVector != 0 && movedataitems.Length > 0) {
                                if(movedataitems[0].MoveVector == moveVector) {
                                    retlist[retlist.Count - 1].AddSize(movedataitems[0].Size);
                                    if(movedataitems.Length > 1) {
                                        Array.Copy(movedataitems, 1, movedataitems, 0, movedataitems.Length - 1);
                                        Array.Resize(ref movedataitems, movedataitems.Length - 1);
                                        retlist.AddRange(movedataitems);
                                    }
                                } else {
                                    retlist[retlist.Count - 1].AddSize(movedataitems[0].Position - blockposition - blockname.Length - 7);
                                    retlist.AddRange(movedataitems);
                                }
                            } else if(movedataitems.Length > 0) retlist.AddRange(movedataitems);
                            moveVector += item.Size - blocksize;
                        } else {            //Если элемент не является секцией
                            if(moveVector != 0 && (retlist.Count == 0 || retlist[retlist.Count-1].MoveVector != moveVector)) {
                                var newmovedata = new DataMoveItem(blockposition, blocksize + 4, moveVector);
                                retlist.Add(newmovedata);
                            } else if(moveVector != 0) retlist[retlist.Count-1].AddSize(blocksize + 4);
                            moveVector += item.Size - blocksize;
                        }
                    } else if( moveVector != 0) { //Если изменений лемента нет, но жанные должны быть смещены
                        if(retlist.Count == 0 || retlist[retlist.Count-1].MoveVector != moveVector) {
                            var newmovedata = new DataMoveItem(blockposition, blocksize + 4, moveVector);
                            retlist.Add(newmovedata);
                        } else retlist[retlist.Count - 1].AddSize(blocksize + 4);
                    }
                    stream.Seek(blockposition + blocksize + 4, SeekOrigin.Begin);
                    ++index;
                } else { //Если новый элемент
                    moveVector += item.SourceSize == 0 ? item.Size + 4 : -(blocksize + 4);
                    stream.Seek(blockposition, SeekOrigin.Begin);
                    if (item.SourceSize == 0) ++index;
                }
            }
            return retlist.ToArray();
        }

        private void SaveSection(Stream stream, ProgressEventCallback progressCallback, ProgressEventArgs eventArgs)
        {
            var position = (int)stream.Position;
            foreach (var item in _items) {
                var blocksize = item.Size;
                var itemposition = (int)stream.Position;
                var nextitemposition = itemposition + blocksize + 4;
                if (item.CanSave && (item.SourceSize == 0 || item.HasChanged)) {
                    stream.Write(BitConverter.GetBytes(blocksize), 0, 4);
                    item.Save(stream, progressCallback, eventArgs);
                    if (item.Type == ConfigurationItemType.Section) position = (int)stream.Position;
                }
                item.SetItemPosition(itemposition, Stream);
                stream.Seek(nextitemposition, SeekOrigin.Begin);
                if (position < stream.Position) eventArgs.AddProgress(stream.Position - position);
                position = (int)stream.Position;
                progressCallback?.Invoke(eventArgs);
            }
        }

        private protected override void SetHasChanged() => HasChanged = true;

        private protected override void ResetHasChanged()
        {
            foreach (IConfigurationItem item in _items)
                item.ResetHasChanged();
            HasChanged = false;
        }

        private protected override int ItemIndex(ConfigurationItem item)
        {
            for (var i = 0; i < _items.Length; ++i)
                if (_items[i] == item) return i;
            return -1;
        }

        #endregion

        #endregion

        #region Свойства

        /// <summary>
        /// Получает указатель, указывающий могут ли вноситься изменения в конфигурацию. 
        /// </summary>
        /// <return>Значенеи true, если изменения могут быть внесены; в противном случае — значение false.</return>
        public override bool CanEdit => !_readonly;

        /// <summary>
        /// Возвращает значение true, если в конфигурации присутствуют несохранённые изменения; в противном случае — значение false
        /// </summary>
        public bool HasChanged { get; private set; } = false;

        internal Stream Stream { get; }

        #endregion

    }
}
