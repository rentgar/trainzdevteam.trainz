﻿using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Globalization;

namespace TrainzDevTeam.Trainz
{
    /// <summary>
    /// Базовый класс для элементов конфигурации.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Globalization", "CA1308")]
    public abstract class ConfigurationItem : IConfigurationItem
    {

        #region Данные класса

        private string _name;                        //Наименвоанеи блока
        private IConfigurationItem _parent;          //Родительский элемент конфигурации
        private string _sourcename;                  //Исходное имя блока

        #endregion

        #region Конструктор

        internal ConfigurationItem(int size, ConfigurationItemType type, string name, int itemPosition, Stream stream)
        {
            Stream = stream;
            ItemPositiom = itemPosition;
            Type = type;
            SourceSize = size;
            _name = name.ToLowerInvariant();
        }

        #endregion

        #region Методы

        /// <summary>
        /// Выполняет проверку, является ли указанная строка допустимым именем элементка конфигурации.
        /// </summary>
        /// <param name="itemName">Наименование элемента конфигурации для проверки</param>
        /// <returns>Значение true, если <paramref name="itemName"/> является допустимым именем элемента конфигурации; в противном случае — значение false</returns>
        /// <remarks>Требования к наименованию ключа конфигурации взяты на http://online.ts2009.com/mediaWiki/index.php/ACS_Text_Format#Keys </remarks>
        public static bool ItemNameIsValid(string itemName) => Regex.IsMatch(itemName, @"^[A-z0-9-_\/]{1,511}$", RegexOptions.Compiled | RegexOptions.Singleline);

        internal void SetParent(IConfigurationItem parent) => _parent = parent;

        internal virtual void Save(Stream stream, ProgressEventCallback progressCallBack, ProgressEventArgs eventArgs)
        {
            byte[] namebytes = Encoding.ASCII.GetBytes(_name);
            stream.WriteByte((byte)(namebytes.Length + 1));
            stream.Write(namebytes, 0, namebytes.Length);
            stream.WriteByte(0);
            stream.WriteByte((byte)Type);
        }

        internal virtual void SetItemPosition(int newPosition, Stream stream)
        {
            ItemPositiom = newPosition;
            Stream = stream;
        }

        internal virtual void SetStream(Stream stream) => Stream = stream;

        protected private virtual void ResetHasChanged() { }

        protected private virtual int ItemIndex(ConfigurationItem item) { return -1; }

        void IConfigurationItem.SetHasChanged()
        {
            _parent.SetHasChanged();
            HasChanged = true;
        }

        void IConfigurationItem.ResetHasChanged()
        {
            SourceSize = Size;
            HasChanged = false;
            ResetHasChanged();
        }

        int IConfigurationItem.ItemIndex(ConfigurationItem item) => ItemIndex(item);

        #endregion

        #region Свойства

        /// <summary>
        /// Возвращает одно из значение перечисления <see cref="ConfigurationItemType"/>, указывающего на типа текущего элемента конфигурации.
        /// </summary>
        public ConfigurationItemType Type { get; }

        /// <summary>
        /// Получает размер данных текущего элемента конфигурации в байтах.
        /// </summary>
        public abstract int DataSize { get; }

        /// <summary>
        /// Получает полный размер текущего элемента конфигурации в байтах.
        /// </summary>
        public int Size => DataSize + _name.Length + 3;

        /// <summary>
        /// Получает исходный размер текущего элемента конфигурации, до внесения в него изменений.
        /// </summary>
        public int SourceSize { get; private set; }

        /// <summary>
        /// Получает или задаёт наименование элемента конфигурации
        /// </summary>
        /// <exception cref="ReadOnlyException">Данные конфигурации доступны только для чтения</exception>
        /// <exception cref="ArgumentNullException">Значение свойства не может быть null</exception>
        /// <exception cref="ArgumentException">Значение свойтсва имеет неверный формат</exception>
        /// <exception cref="AlreadyExistException">Другой элемент конфигурации с указанным именем уже существует в коллекции <see cref="ParentCollection"/></exception>
        public string Name
        {
            get => _name;
            set {
                if (!CanEdit) throw new ReadOnlyException(ResourceStrings.Exception_ConfigurationReadOnly);
                if (value == null) throw new ArgumentNullException(nameof(value));
                if (!ItemNameIsValid(value)) throw new ArgumentException(ResourceStrings.Exception_ConfigurationKeyName, nameof(value));
                var otheritem = ParentCollection?.FindItem(value);
                if (otheritem != null && otheritem != this) throw new AlreadyExistException(string.Format(CultureInfo.CurrentCulture, ResourceStrings.Exception_ConfigurationItemAlreadyExist, value));
                value = value.ToLowerInvariant();
                if (value != _name) {
                    if (string.IsNullOrWhiteSpace(_sourcename)) _sourcename = _name;
                    _name = value;
                    ((IConfigurationItem)this).SetHasChanged();
                }
            }
        }


        /// <summary>
        /// Возвращает значение true, если текущий элемент конфигурации имеет несохранённые изменения; в противном случае — значение false.
        /// </summary>
        public bool HasChanged { get; private set; }

        /// <summary>
        /// Возвращает отсчитываемый от нуля индекс текущего элемента конфигурации в родительском элементе конфигурации, или -1, если текущий элемент конфигурации не имеет родительского элемента конфигурации.
        /// </summary>
        public int Index => (_parent != null) ? _parent.ItemIndex(this) : -1;

        /// <summary>
        /// Возвращает объект <see cref="SectionConfigurationItem"/>, представляющий родительский элемент конфигурации для текущего элемента конфигураии; или значение null, если текущий элемент конфигурации не входит в другой элемент.
        /// </summary>
        public SectionConfigurationItem Parent => _parent as SectionConfigurationItem;

        /// <summary>
        /// Вовзращает объект с интерфейсом <see cref="IConfigurationItemCollection"/>, представляющий коллекуию элементов конфигурации, в которую входит текущий элемент конфигурации; или значение null, если текущий элемент конфигурации не является частью коллекции элементов.
        /// </summary>
        public IConfigurationItemCollection ParentCollection => _parent as IConfigurationItemCollection;


        internal string SourceName => !string.IsNullOrWhiteSpace(_sourcename) ? _sourcename : Name;

        internal int ItemPositiom { get; private set; }

        internal Stream Stream { get; private set; }

        internal virtual bool CanSave => true;

        protected private bool CanEdit => _parent?.CanEdit ?? true;

        bool IConfigurationItem.CanEdit => CanEdit;

        #endregion

    }

}
