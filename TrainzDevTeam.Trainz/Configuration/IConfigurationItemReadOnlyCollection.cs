﻿using System.Collections.Generic;

namespace TrainzDevTeam.Trainz
{
    /// <summary>
    /// Описывает доступную только для чтения коллекцию элементов конфигурации.
    /// </summary>
    public interface IConfigurationItemReadOnlyCollection : IEnumerable<ConfigurationItem>
    {

        /// <summary>
        /// Выполняет поиск элемента конфигурации.
        /// </summary>
        /// <param name="itemName">Имя элемента конфигурации, который необходимо найти.</param>
        /// <returns>Объект <see cref="ConfigurationItem"/>, представляющий найденный элемент конфигурации; или значение null, если элемент не был найден.</returns>
        ConfigurationItem FindItem(string itemName);

        /// <summary>
        /// Выполняет поиск индекса для входящего элемента конфигурации.
        /// </summary>
        /// <param name="item">Объект <see cref="ConfigurationItem"/>, содержащий данные элемента конфигурации индекс которого необходимо найти.</param>
        /// <returns>Отсчитываемый от нуля индекс элемента конфигурации или -1 — если элемент не был найден.</returns>
        int IndexOf(ConfigurationItem item);

        /// <summary>
        /// Выолняет поиск индекса для входящего элемента конфигурации.
        /// </summary>
        /// <param name="itemName">Наименование элемента конфигурации, который необходимо найти.</param>
        /// <returns>Отсчитываемый от нуля индекс элемента конфигурации или -1 — если элемент не был найден.</returns>
        int IndexOf(string itemName);

        /// <summary>
        /// Определяет, является ли указанный элемент конфигурации частью текущей секции.
        /// </summary>
        /// <param name="item">Объект <see cref="ConfigurationItem"/>, содержащий данные элемента конфигурации который необходимо проверить.</param>
        /// <returns>Значение true, если <paramref name="item"/> является частью текущей секции; в противном случае — значение false.</returns>
        bool Contains(ConfigurationItem item);

        /// <summary>
        /// Определяет, является ли элемент конфигурации с указанным именем частью текущей секции.
        /// </summary>
        /// <param name="itemName">Наименование элемента конфигурации, входимость которого необходимо проверить.</param>
        /// <returns>Значение true, если элемен колекции с наименованием <paramref name="itemName"/> является частью текущей секции; в противном случае — значение false.</returns>
        bool Contains(string itemName);

        /// <summary>
        /// Получает количество элементов конфигурации в текущей секции.
        /// </summary>
        int Count { get; }

        /// <summary>
        /// Получение элемента конфигурации в текущей секции по его индексу.
        /// </summary>
        /// <param name="index">Отсчитываемый от нуля индекс элемента конфигурации в текущей секции.</param>
        /// <returns>Объект <see cref="ConfigurationItem"/>, содержащий данные элемента конфигурации.</returns>
        ConfigurationItem this[int index] { get; }

    }
}
