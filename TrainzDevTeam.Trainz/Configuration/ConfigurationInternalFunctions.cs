﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

namespace TrainzDevTeam.Trainz
{
    internal static class ConfigurationInternalFunctions
    {

        public static ConfigurationItem[] ParseConfigurationDataBlock(Stream stream, int blockPosition, int blockSize, ProgressEventCallback progressCallback, ProgressEventArgs eventArgs)
        {
            var retitems = new List<ConfigurationItem>();
            var maxposition = blockPosition + blockSize + 4;// (int)stream.Position + blockSize;
            while (stream.Position < maxposition) {
                var position = (int)stream.Position;
                var itemsize = stream.ReadInt32();
                if (itemsize > blockSize || position + itemsize + 4 > maxposition) 
                    throw new BadFormatException(ResourceStrings.Exception_ErrorConfigurationDataFormat);
                var itemname = ReadConfigurationDataItemName(stream);
                ConfigurationItemType itemtype = (ConfigurationItemType)stream.ReadByte();
                switch (itemtype) {

                    case ConfigurationItemType.Section:
                        ConfigurationItem[] items = ParseConfigurationDataBlock(stream, position, itemsize, progressCallback, eventArgs);
                        retitems.Add(new SectionConfigurationItem(itemsize, itemtype, itemname, position, stream, items));
                        eventArgs.AddProgress(itemname.Length + 3);
                        break;

                    case ConfigurationItemType.Int:
                        retitems.Add(new IntConfigurationItem(itemsize, itemtype, itemname, position, stream));
                        eventArgs.AddProgress(itemsize);
                        break;

                    case ConfigurationItemType.Float:
                        retitems.Add(new FloatConfigurationItem(itemsize, itemtype, itemname, position, stream));
                        eventArgs.AddProgress(itemsize);
                        break;

                    case ConfigurationItemType.String:
                        retitems.Add(new StringConfigurationItem(itemsize, itemtype, itemname, position, stream));
                        eventArgs.AddProgress(itemsize);
                        break;

                    case ConfigurationItemType.FileData:
                        retitems.Add(new FileConfigurationItem(itemsize, itemtype, itemname, position, stream));
                        stream.Seek(retitems[retitems.Count - 1].DataSize, SeekOrigin.Current);
                        eventArgs.AddProgress(itemsize);
                        break;

                    case ConfigurationItemType.Empty:
                        retitems.Add(new EmptyConfigurationItem(itemsize, itemtype, itemname, position, stream));
                        stream.Seek(retitems[retitems.Count - 1].DataSize, SeekOrigin.Current); //Заглушка на всякий случай
                        eventArgs.AddProgress(itemsize);
                        break;

                    case ConfigurationItemType.Kuid:
                        retitems.Add(new KuidConfigurationItem(itemsize, itemtype, itemname, position, stream));
                        eventArgs.AddProgress(itemsize);
                        break;

                    default:
                        retitems.Add(new UnknownConfigurationItem(itemsize, itemtype, itemname, position, stream));
                        stream.Seek(retitems[retitems.Count - 1].DataSize, SeekOrigin.Current);
                        eventArgs.AddProgress(itemsize);
                        break;
                }
                progressCallback?.Invoke(eventArgs);
            }
            return retitems.ToArray();
        }

        public static string ReadConfigurationDataItemName(Stream stream)
        {
            var buffer = new byte[stream.ReadByte() - 1];
            stream.Read(buffer, 0, buffer.Length);
            stream.Seek(1, SeekOrigin.Current);
            return Encoding.ASCII.GetString(buffer);
        }




        public static void SaveConfigurationItem(ConfigurationItem item, Stream stream, ProgressEventCallback progressCallback, ProgressEventArgs eventArgs)
        {
            if (item.HasChanged || item.Stream == null) {
                var itemsize = item.Size;
                stream.Write(BitConverter.GetBytes(itemsize), 0, 4);
                if (item.Type == ConfigurationItemType.Section) {
                    var section = (SectionConfigurationItem)item;
                    section.SaveOnlyHeader(stream, null, eventArgs);
                    foreach (ConfigurationItem subitem in section)
                        SaveConfigurationItem(subitem, stream, progressCallback, eventArgs);
                    eventArgs.AddProgress(item.Name.Length + 7);
                    progressCallback?.Invoke(eventArgs);
                } else {
                    item.Save(stream, null, eventArgs);
                    eventArgs.AddProgress(itemsize);
                    progressCallback?.Invoke(eventArgs);
                }
            } else {
                eventArgs.AddProgress(CopyConfigurationItem(item, stream));
                progressCallback?.Invoke(eventArgs);
            }
        }

        public static int CopyConfigurationItem(ConfigurationItem item, Stream stream)
        {
            var copysize = item.Size + 4;
            var buffer = new byte[Math.Min(copysize, 4194304)];
            var copieddatasize = 0;
            item.Stream.Seek(item.ItemPositiom, SeekOrigin.Begin);
            while (copieddatasize < copysize) {
                var datasize = Math.Min(copysize - copieddatasize, buffer.Length);
                item.Stream.Read(buffer, 0, datasize);
                stream.Write(buffer, 0, datasize);
                copieddatasize += datasize;
            }
            return copysize;
        }


    }
}
