﻿using System;
using System.Globalization;

namespace TrainzDevTeam.Trainz
{
    /// <summary>
    /// Базовый класс для описания редактируемой структуры данных конфигурации.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1710")]
    public abstract class EditableConfiguration : ReadableConfiguration, IConfigurationItemCollection
    {

        #region Данные класса

        protected private int _version = 0;

        #endregion

        #region Конструктор

        /// <summary>
        /// Инициализаирует новый экземпляр класса <see cref="EditableConfiguration"/>
        /// </summary>
        protected private EditableConfiguration() : base() { }

        #endregion

        #region Методы

        /// <summary>
        /// Добавляет новую секцию в коллекцию.
        /// </summary>
        /// <param name="itemName">Наименование добавляемого эемента конфигурации.</param>
        /// <returns>Объект <see cref="SectionConfigurationItem"/>, представляющий новый элемент конфигурации.</returns>
        /// <exception cref="ReadOnlyException">Данные конфигурации доступны только для чтения.</exception>
        /// <exception cref="ArgumentNullException">Аргумент <paramref name="itemName"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="itemName"/> содержит недопустимое значение.</exception>
        /// <exception cref="AlreadyExistException">Другой элемент конфигурации с наименованием <paramref name="itemName"/> уже присутствует в коллекции.</exception>
        public SectionConfigurationItem AddItem(string itemName)
        {
            if (!CanEdit) throw new ReadOnlyException(ResourceStrings.Exception_ConfigurationReadOnly);
            if (itemName == null) throw new ArgumentNullException(nameof(itemName));
            if (!ConfigurationItem.ItemNameIsValid(itemName)) throw new ArgumentException(ResourceStrings.Exception_ConfigurationKeyName, nameof(itemName));
            if (Contains(itemName)) throw new AlreadyExistException(string.Format(CultureInfo.CurrentCulture, ResourceStrings.Exception_ConfigurationItemAlreadyExist, itemName));
            var newitem = new SectionConfigurationItem(itemName);
            Array.Resize(ref _items, _items.Length + 1);
            _items[_items.Length - 1] = newitem;
            newitem.SetParent(this);
            ((IConfigurationItem)this).SetHasChanged();
            _version++;
            return newitem;
        }

        /// <summary>
        /// Добавляет новый элемент конфигурации содержащий целочисленные данные.
        /// </summary>
        /// <param name="itemName">Наименование добавляемого эемента конфигурации.</param>
        /// <param name="values">Значения элемента конфигурации в количестве от 1 до 255.</param>
        /// <returns>Объект <see cref="IntConfigurationItem"/>, представляющий новый элемент конфигурации.</returns>
        /// <exception cref="ReadOnlyException">Данные конфигурации доступны только для чтения.</exception>
        /// <exception cref="ArgumentNullException">Аргумент <paramref name="itemName"/> не может быть null. -или- Аргумент <paramref name="values"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="itemName"/> содержит недопустимое значение. -или- Аргумент <paramref name="values"/> должен иметь от 1 до 255 элементов.</exception>
        /// <exception cref="AlreadyExistException">Другой элемент конфигурации с наименованием <paramref name="itemName"/> уже присутствует в коллекции.</exception>
        public IntConfigurationItem AddItem(string itemName, params int[] values)
        {
            if (!CanEdit) throw new ReadOnlyException(ResourceStrings.Exception_ConfigurationReadOnly);
            if (itemName == null) throw new ArgumentNullException(nameof(itemName));
            if (!ConfigurationItem.ItemNameIsValid(itemName)) throw new ArgumentException(ResourceStrings.Exception_ConfigurationKeyName, nameof(itemName));
            if (values == null) throw new ArgumentNullException(nameof(values));
            if (values.Length < 1 || values.Length > 255) throw new ArgumentException(ResourceStrings.Exception_ConfigurationItemArrayValue, nameof(values));
            if (Contains(itemName)) throw new AlreadyExistException(string.Format(CultureInfo.CurrentCulture, ResourceStrings.Exception_ConfigurationItemAlreadyExist, itemName));
            var newitem = new IntConfigurationItem(itemName, values);
            Array.Resize(ref _items, _items.Length + 1);
            _items[_items.Length - 1] = newitem;
            newitem.SetParent(this);
            ((IConfigurationItem)this).SetHasChanged();
            _version++;
            return newitem;
        }

        /// <summary>
        /// Добавляет новый элемент конфигурации содержащий значения с плавающей точкой.
        /// </summary>
        /// <param name="itemName">Наименование добавляемого эемента конфигурации.</param>
        /// <param name="values">Значения элемента конфигурации в количестве от 1 до 255.</param>
        /// <returns>Объект <see cref="FloatConfigurationItem"/>, представляющий новый элемент конфигурации.</returns>
        /// <exception cref="ReadOnlyException">Данные конфигурации доступны только для чтения.</exception>
        /// <exception cref="ArgumentNullException">Аргумент <paramref name="itemName"/> не может быть null. -или- Аргумент <paramref name="values"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="itemName"/> содержит недопустимое значение. -или- Аргумент <paramref name="values"/> должен иметь от 1 до 255 элементов.</exception>
        /// <exception cref="AlreadyExistException">Другой элемент конфигурации с наименованием <paramref name="itemName"/> уже присутствует в коллекции.</exception>
        public FloatConfigurationItem AddItem(string itemName, params float[] values)
        {
            if (!CanEdit) throw new ReadOnlyException(ResourceStrings.Exception_ConfigurationReadOnly);
            if (itemName == null) throw new ArgumentNullException(nameof(itemName));
            if (!ConfigurationItem.ItemNameIsValid(itemName)) throw new ArgumentException(ResourceStrings.Exception_ConfigurationKeyName, nameof(itemName));
            if (values == null) throw new ArgumentNullException(nameof(values));
            if (values.Length < 1 || values.Length > 255) throw new ArgumentException(ResourceStrings.Exception_ConfigurationItemArrayValue, nameof(values));
            if (Contains(itemName)) throw new AlreadyExistException(string.Format(CultureInfo.CurrentCulture, ResourceStrings.Exception_ConfigurationItemAlreadyExist, itemName));
            var newitem = new FloatConfigurationItem(itemName, values);
            Array.Resize(ref _items, _items.Length + 1);
            _items[_items.Length - 1] = newitem;
            newitem.SetParent(this);
            ((IConfigurationItem)this).SetHasChanged();
            _version++;
            return newitem;
        }

        /// <summary>
        /// Добавляет новый элемент конфигурации содержащий строкове значение.
        /// </summary>
        /// <param name="itemName">Наименование добавляемого эемента конфигурации.</param>
        /// <param name="value">Строка, которая будет являться значением элемента конфигурации.</param>
        /// <returns>Объект <see cref="StringConfigurationItem"/>, представляющий новый элемент конфигурации.</returns>
        /// <exception cref="ReadOnlyException">Данные конфигурации доступны только для чтения.</exception>
        /// <exception cref="ArgumentNullException">Аргумент <paramref name="itemName"/> не может быть null. -или- Аргумент <paramref name="value"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="itemName"/> содержит недопустимое значение.</exception>
        /// <exception cref="AlreadyExistException">Другой элемент конфигурации с наименованием <paramref name="itemName"/> уже присутствует в коллекции.</exception>
        public StringConfigurationItem AddItem(string itemName, string value)
        {
            if (!CanEdit) throw new ReadOnlyException(ResourceStrings.Exception_ConfigurationReadOnly);
            if (itemName == null) throw new ArgumentNullException(nameof(itemName));
            if (!ConfigurationItem.ItemNameIsValid(itemName)) throw new ArgumentException(ResourceStrings.Exception_ConfigurationKeyName, nameof(itemName));
            if (value == null) throw new ArgumentNullException(nameof(value));
            if (Contains(itemName)) throw new AlreadyExistException(string.Format(CultureInfo.CurrentCulture, ResourceStrings.Exception_ConfigurationItemAlreadyExist, itemName));
            var newitem = new StringConfigurationItem(itemName, value);
            Array.Resize(ref _items, _items.Length + 1);
            _items[_items.Length - 1] = newitem;
            newitem.SetParent(this);
            ((IConfigurationItem)this).SetHasChanged();
            _version++;
            return newitem;
        }

        /// <summary>
        /// Добавляет новый элемент конфигурации содержащий значение типа KUID.
        /// </summary>
        /// <param name="itemName">Наименование добавляемого эемента конфигурации.</param>
        /// <param name="value">Экземпляр структуры <see cref="Kuid"/>, содержащий KUID номер для элемента конфигурации.</param>
        /// <returns>Объект <see cref="KuidConfigurationItem"/>, представляющий новый элемент конфигурации.</returns>
        /// <exception cref="ReadOnlyException">Данные конфигурации доступны только для чтения.</exception>
        /// <exception cref="ArgumentNullException">Аргумент <paramref name="itemName"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="itemName"/> содержит недопустимое значение.</exception>
        /// <exception cref="AlreadyExistException">Другой элемент конфигурации с наименованием <paramref name="itemName"/> уже присутствует в коллекции.</exception>
        public KuidConfigurationItem AddItem(string itemName, Kuid value)
        {
            if (!CanEdit) throw new ReadOnlyException(ResourceStrings.Exception_ConfigurationReadOnly);
            if (itemName == null) throw new ArgumentNullException(nameof(itemName));
            if (!ConfigurationItem.ItemNameIsValid(itemName)) throw new ArgumentException(ResourceStrings.Exception_ConfigurationKeyName, nameof(itemName));
            if (Contains(itemName)) throw new AlreadyExistException(string.Format(CultureInfo.CurrentCulture, ResourceStrings.Exception_ConfigurationItemAlreadyExist, itemName));
            var newitem = new KuidConfigurationItem(itemName, value);
            Array.Resize(ref _items, _items.Length + 1);
            _items[_items.Length - 1] = newitem;
            newitem.SetParent(this);
            ((IConfigurationItem)this).SetHasChanged();
            _version++;
            return newitem;
        }

        /// <summary>
        /// Добавляет новый пустой элемент конфигурации.
        /// </summary>
        /// <param name="itemName">Наименование добавляемого эемента конфигурации.</param>
        /// <returns>Объект <see cref="EmptyConfigurationItem"/>, представляющий новый элемент конфигурации.</returns>
        /// <exception cref="ReadOnlyException">Данные конфигурации доступны только для чтения.</exception>
        /// <exception cref="ArgumentNullException">Аргумент <paramref name="itemName"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="itemName"/> содержит недопустимое значение.</exception>
        /// <exception cref="AlreadyExistException">Другой элемент конфигурации с наименованием <paramref name="itemName"/> уже присутствует в коллекции.</exception>
        public EmptyConfigurationItem AddEmptyItem(string itemName)
        {
            if (!CanEdit) throw new ReadOnlyException(ResourceStrings.Exception_ConfigurationReadOnly);
            if (itemName == null) throw new ArgumentNullException(nameof(itemName));
            if (!ConfigurationItem.ItemNameIsValid(itemName)) throw new ArgumentException(ResourceStrings.Exception_ConfigurationKeyName, nameof(itemName));
            if (Contains(itemName)) throw new AlreadyExistException(string.Format(CultureInfo.CurrentCulture, ResourceStrings.Exception_ConfigurationItemAlreadyExist, itemName));
            var newitem = new EmptyConfigurationItem(itemName);
            Array.Resize(ref _items, _items.Length + 1);
            _items[_items.Length - 1] = newitem;
            newitem.SetParent(this);
            ((IConfigurationItem)this).SetHasChanged();
            _version++;
            return newitem;
        }

        /// <summary>
        /// Выполняет удаление элемента конфигурации из текущей конфигурации.
        /// </summary>
        /// <param name="item">Объект <see cref="ConfigurationItem"/>, представляющий удаляемый элемента конфигурации.</param>
        /// <returns>Значение true, если элемент конфигурации был удачно удалён; в противном случае — значение false.</returns>
        /// <exception cref="ReadOnlyException">Данные конфигурации доступны только для чтения.</exception>
        /// <exception cref="ArgumentNullException">Аргумент <paramref name="item"/> не может быть null.</exception>
        public bool RemoveItem(ConfigurationItem item)
        {
            var index = IndexOf(item);
            if (index >= 0) RemoveItem(index);
            return index >= 0;
        }

        /// <summary>
        /// Выполняет удаление элемента конфигурации из текущей конфигурации.
        /// </summary>
        /// <param name="itemName">Наименование элемента конфигурации, который необходимо удалить.</param>
        /// <returns>Значение true, если элемент конфигурации был удачно удалён; в противном случае — значение false.</returns>
        /// <exception cref="ReadOnlyException">Данные конфигурации доступны только для чтения.</exception>
        /// <exception cref="ArgumentNullException">Аргумент <paramref name="itemName"/> не может быть null.</exception>
        /// <exception cref="ArgumentException">Аргумент <paramref name="itemName"/> содержит недопустимое значение.</exception>
        public bool RemoveItem(string itemName)
        {
            var index = IndexOf(itemName);
            if (index >= 0) RemoveItem(index);
            return index >= 0;
        }

        /// <summary>
        /// Выполняет удаление элемента конфигурации из текущей конфигурации.
        /// </summary>
        /// <param name="index">Отсчитываемый он нуля индекс удаляемого элемента конфигурации в текущей секции.</param>
        /// <exception cref="ReadOnlyException">Данные конфигурации доступны только для чтения.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Значение аргумента <paramref name="index"/> меньше 0. -или- Значение аргумента <paramref name="index"/> равно или больше значения свойства <see cref="ReadableConfiguration.Count"/>.</exception>
        public void RemoveItem(int index)
        {
            if (!CanEdit) throw new ReadOnlyException(ResourceStrings.Exception_ConfigurationReadOnly);
            if (index < 0 || index >= _items.Length) throw new ArgumentOutOfRangeException(nameof(index), ResourceStrings.Exception_IndexOutOfRange);
            _items[index].SetParent(null);
            _items[index].SetStream(null);
            if (index < _items.Length - 1) Array.Copy(_items, index + 1, _items, index, _items.Length - index - 1);
            Array.Resize(ref _items, _items.Length - 1);
            ((IConfigurationItem)this).SetHasChanged();
            _version++;
        }

        #endregion

        #region Свойства

        /// <summary>
        /// Получает указатель, указывающий могут ли вноситься изменения в конфигурацию. 
        /// </summary>
        /// <return>Значенеи true, если изменения могут быть внесены; в противном случае — значение false.</return>
        public override bool CanEdit => true;

#pragma warning disable CS1591 
        
        protected override int CollectionVersion => _version;

#pragma warning restore CS1591 

        #endregion

    }
}
