﻿using System.Collections.Generic;

namespace TrainzDevTeam.Trainz
{
    /// <summary>
    /// Определяет интерфейс колекции элементов конфигурации.
    /// </summary>
    public interface IConfigurationItemCollection : IEnumerable<ConfigurationItem>
    {

        /// <summary>
        /// Выполняет поиск элемента конфигурации.
        /// </summary>
        /// <param name="itemName">Имя элемента конфигурации, который необходимо найти.</param>
        /// <returns>Объект <see cref="ConfigurationItem"/>, представляющий найденный элемент конфигурации; или значение null, если элемент не был найден.</returns>
        ConfigurationItem FindItem(string itemName);

        /// <summary>
        /// Выполняет добавление новой секции в конфигурацию.
        /// </summary>
        /// <param name="itemName">Наименвоание добавляемого элемента конфигурации.</param>
        /// <returns>Объект <see cref="SectionConfigurationItem"/>, содержащий данные добавленной секции конфигурации.</returns>
        SectionConfigurationItem AddItem(string itemName);

        /// <summary>
        /// Добавление нового элемента конфигурации содержащего целочисленные значения.
        /// </summary>
        /// <param name="itemName">Наименвоание добавляемого элемента конфигурации.</param>
        /// <param name="values">Значения элемента конфигурации в количестве от 1 до 256.</param>
        /// <returns>Объект <see cref="IntConfigurationItem"/>, содержащий данные добавленого элемента конфигурации.</returns>
        IntConfigurationItem AddItem(string itemName, params int[] values);

        /// <summary>
        /// Добавление нового элемента конфигурации содержащего значения с плавающей точкой.
        /// </summary>
        /// <param name="itemName">Наименвоание добавляемого элемента конфигурации.</param>
        /// <param name="values">Значения элемента конфигурации в количестве от 1 до 256.</param>
        /// <returns>Объект <see cref="FloatConfigurationItem"/>, содержащий данные добавленого элемента конфигурации.</returns>
        FloatConfigurationItem AddItem(string itemName, params float[] values);

        /// <summary>
        /// Добавление нового элемента конфигурации содержащего строковое начение.
        /// </summary>
        /// <param name="itemName">Наименвоание добавляемого элемента конфигурации.</param>
        /// <param name="value">Строка, которая будет являться значением элемента конфигурации.</param>
        /// <returns>Объект <see cref="StringConfigurationItem"/>, содержащий данные добавленого элемента конфигурации.</returns>
        StringConfigurationItem AddItem(string itemName, string value);

        /// <summary>
        /// Добавление нового элемента конфигурации содержащего значение типа KUID.
        /// </summary>
        /// <param name="itemName">Наименвоание добавляемого элемента конфигурации.</param>
        /// <param name="value">Объект <see cref="Kuid"/>, содержащий данные для значения элемента конфигурации.</param>
        /// <returns>Объект <see cref="KuidConfigurationItem"/>, содержащий данные добавленого элемента конфигурации.</returns>
        KuidConfigurationItem AddItem(string itemName, Kuid value);

        /// <summary>
        /// Выполняет поиск индекса для входящего элемента конфигурации.
        /// </summary>
        /// <param name="item">Объект <see cref="ConfigurationItem"/>, содержащий данные элемента конфигурации индекс которого необходимо найти.</param>
        /// <returns>Отсчитываемый от нуля индекс элемента конфигурации или -1 — если элемент не был найден.</returns>
        int IndexOf(ConfigurationItem item);

        /// <summary>
        /// Выолняет поиск индекса для входящего элемента конфигурации.
        /// </summary>
        /// <param name="itemName">Наименование элемента конфигурации, который необходимо найти.</param>
        /// <returns>Отсчитываемый от нуля индекс элемента конфигурации или -1 — если элемент не был найден.</returns>
        int IndexOf(string itemName);

        /// <summary>
        /// Определяет, является ли указанный элемент конфигурации частью текущей секции.
        /// </summary>
        /// <param name="item">Объект <see cref="ConfigurationItem"/>, содержащий данные элемента конфигурации который необходимо проверить.</param>
        /// <returns>Значение true, если <paramref name="item"/> является частью текущей секции; в противном случае — значение false.</returns>
        bool Contains(ConfigurationItem item);

        /// <summary>
        /// Определяет, является ли элемент конфигурации с указанным именем частью текущей секции.
        /// </summary>
        /// <param name="itemName">Наименование элемента конфигурации, входимость которого необходимо проверить.</param>
        /// <returns>Значение true, если элемен колекции с наименованием <paramref name="itemName"/> является частью текущей секции; в противном случае — значение false.</returns>
        bool Contains(string itemName);

        /// <summary>
        /// Выполняет удаление элемента конфигурации их текущей коллекции.
        /// </summary>
        /// <param name="item">Объект <see cref="ConfigurationItem"/>, осдержащий данные удаляемого элемента конфигурации.</param>
        /// <returns>Значение true, если элемент конфигурации был удачно удалён; в противном случае — значение false.</returns>
        bool RemoveItem(ConfigurationItem item);

        /// <summary>
        /// Выполняет удаление элемента конфигурации их текущей коллекции.
        /// </summary>
        /// <param name="itemName">Наименование элемента конфигурации, который необходимо удалить.</param>
        /// <returns>Значение true, если элемент конфигурации был удачно удалён; в противном случае — значение false.</returns>
        bool RemoveItem(string itemName);

        /// <summary>
        /// Выполняет удаление элемента конфигурации их текущей коллекции.
        /// </summary>
        /// <param name="index">Отсчитываемый от нуля индекс элемента конфигурации в текущей коллекции.</param>
        void RemoveItem(int index);

        /// <summary>
        /// Получает количество элементов конфигурации в текущей коллекции.
        /// </summary>
        int Count { get; }

        /// <summary>
        /// Получение элемента конфигурации в текущей секции по его индексу.
        /// </summary>
        /// <param name="index">Отсчитываемый от нуля индекс элемента конфигурации в текущей коллекции.</param>
        /// <returns>Объект <see cref="ConfigurationItem"/>, содержащий данные элемента конфигурации.</returns>
        ConfigurationItem this[int index] { get; }

    }
}
