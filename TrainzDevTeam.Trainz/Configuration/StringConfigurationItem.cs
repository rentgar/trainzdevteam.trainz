﻿using System;
using System.IO;
using System.Text;
using System.Globalization;

namespace TrainzDevTeam.Trainz
{
    /// <summary>
    /// Описывает элемент конфигурации содержащий строковые данные.
    /// </summary>
    public sealed class StringConfigurationItem : ConfigurationItem
    {

        #region Данные класса

        private byte[] _stringdata;

        #endregion

        #region Конструктор

        internal StringConfigurationItem(int size, ConfigurationItemType type, string name, int itemPosition, Stream stream) : base(size, type, name, itemPosition, stream)
        {
            int length = size - name.Length - 4;
            if (length > 0) {
                _stringdata = new byte[length];
                stream.Read(_stringdata, 0, length);
            } else _stringdata = Array.Empty<byte>();
            stream.Seek(1, SeekOrigin.Current);
        }

        internal StringConfigurationItem(string name, string value) : base(0, ConfigurationItemType.String, name, -1, null) => _stringdata = Encoding.UTF8.GetBytes(value);

        #endregion

        #region Свойства

        /// <summary>
        /// Получает размер данных текущего элемента конфигурации в байтах.
        /// </summary>
        public override int DataSize => _stringdata.Length + 1;

        /// <summary>
        /// Получает или задаёт значение текщего элемента конфигурации.
        /// </summary>
        /// <exception cref="ReadOnlyException">Данные конфигурации доступны только для чтения.</exception>
        public string Value
        {
            get => Encoding.UTF8.GetString(_stringdata);
            set {
                if (!CanEdit) throw new ReadOnlyException(ResourceStrings.Exception_ConfigurationReadOnly);
                if (value != Value) {
                    _stringdata = Encoding.UTF8.GetBytes(value);
                    ((IConfigurationItem)this).SetHasChanged();
                }
            }
        }

        #endregion

        #region Методы

        /// <summary>
        /// Возвращает строку, представляющую текущий элемент конфигурации.
        /// </summary>
        public override string ToString() => string.Format(CultureInfo.InvariantCulture, "{0} = \"{1}\"", Name, Value);

        internal override void Save(Stream stream, ProgressEventCallback progressCallBack, ProgressEventArgs eventArgs)
        {
            base.Save(stream, progressCallBack, eventArgs);
            stream.Write(_stringdata, 0, _stringdata.Length);
            stream.WriteByte(0);
        }

        #endregion

    }
}
