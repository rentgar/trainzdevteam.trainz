﻿using System;
using System.IO;
using System.Globalization;

namespace TrainzDevTeam.Trainz
{
    
    /// <summary>
    /// Описывает элемент конфигурации содержащий KUID.
    /// </summary>
    public sealed class KuidConfigurationItem : ConfigurationItem
    {

        #region Данные

        private Kuid _kuid;

        #endregion

        #region Конструктор

        internal KuidConfigurationItem(int size, ConfigurationItemType type, string name, int itemPosition, Stream stream) : base(size, type, name, itemPosition, stream)
        {
            byte[] buffer = new byte[8];
            stream.Read(buffer, 0, 8);
            _kuid = new Kuid(BitConverter.ToInt64(buffer, 0));
        }

        internal KuidConfigurationItem(string name, Kuid value) : base(0, ConfigurationItemType.String, name, -1, null) => _kuid = value;

        #endregion

        #region Методы

        /// <summary>
        /// Возвращает строку, представляющую текущий элемент конфигурации.
        /// </summary>
        /// <returns>Cтрока, представляющая текущий элемент конфигурации.</returns>
        public override string ToString() => string.Format(CultureInfo.InvariantCulture, "{0} = {1}", Name, _kuid.ToString(true));

        internal override void Save(Stream stream, ProgressEventCallback progressCallBack, ProgressEventArgs eventArgs)
        {
            base.Save(stream, progressCallBack, eventArgs);
            byte[] buffer = BitConverter.GetBytes(_kuid.Packaging());
            stream.Write(buffer, 0, buffer.Length);
        }

        #endregion

        #region Свойства

        /// <summary>
        /// Получает размер данных текущего элемента конфигурации в байтах.
        /// </summary>
        public override int DataSize => 8;

        /// <summary>
        /// Получает или задаёт экземпляр структуры <see cref="Kuid"/>, являющийся значением текущего элемента конфигурации.
        /// </summary>
        /// <exception cref="ReadOnlyException">Данные конфигурации доступны только для чтения.</exception>
        public Kuid Value
        {
            get { return _kuid; }
            set {
                if (!CanEdit) throw new ReadOnlyException(ResourceStrings.Exception_ConfigurationReadOnly);
                if (value != _kuid) {
                    _kuid = value;
                    ((IConfigurationItem)this).SetHasChanged();
                }
            }
        }

        #endregion

    }
}
