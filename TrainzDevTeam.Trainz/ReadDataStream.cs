﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Globalization;

namespace TrainzDevTeam.Trainz
{

    internal sealed class ReadDataStream : Stream
    {

        #region Данные класса

        private Stream _basestream;
        private int _index;
        private int _length;
        private int _currentposition = 0;

        #endregion

        #region Конструктор

        internal ReadDataStream(Stream baseStream, int index, int length)
        {
            _basestream = baseStream;
            _index = index;
            _length = length;
        }

        #endregion

        #region Свойства

        public override bool CanRead => true;

        public override bool CanSeek => true;

        public override bool CanWrite => false;

        public override bool CanTimeout => false;

        public override long Length => _length;

        public override long Position
        {
            get => _currentposition;
            set
            {
                if (value < 0 || value >= _length) throw new IOException(ResourceStrings.Exception_IndexOutOfStream);
                _currentposition = (int)value;
            }
        }

        #endregion

        #region Методы

        public override int Read(byte[] buffer, int offset, int count)
        {
            if (buffer == null) throw new ArgumentNullException(nameof(buffer));
            if (offset < 0) throw new ArgumentOutOfRangeException(nameof(offset), ResourceStrings.Exception_ValueCannotNegative);
            if (count < 0) throw new ArgumentOutOfRangeException(nameof(count), ResourceStrings.Exception_ValueCannotNegative);
            if (offset + count > buffer.Length) throw new ArgumentException(string.Format(CultureInfo.CurrentCulture, ResourceStrings.Exception_OffsetCountOutOfRange, nameof(offset), nameof(count)));
            int copylength = Math.Min(count, _length - _currentposition);
            if (copylength > 0) {
                _basestream.Seek(_index + _currentposition, SeekOrigin.Begin);
                copylength = _basestream.Read(buffer, offset, copylength);
                _currentposition += copylength;
            }
            return copylength;
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            if (!Enum.IsDefined(typeof(SeekOrigin), origin)) throw new ArgumentException(string.Format(CultureInfo.CurrentCulture, ResourceStrings.Exception_WrongArgumentType, typeof(SeekOrigin).FullName, nameof(origin)));
            _currentposition = ((origin == SeekOrigin.End) ? _length : ((origin == SeekOrigin.Current) ? _currentposition : 0)) + (int)offset;
            _currentposition = Math.Max(Math.Min(_currentposition, _length), 0);
            return _currentposition;
        }

        public byte[] ToArray()
        {
            byte[] buffer = new byte[_length];
            _basestream.Seek(_index, SeekOrigin.Begin);
            _basestream.Read(buffer, 0, buffer.Length);
            return buffer;
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public override void Flush()
        {
            throw new NotSupportedException(ResourceStrings.Exception_StreamOperationNotSupport);
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public override void Write(byte[] buffer, int offset, int count)
        {
            throw new NotSupportedException(ResourceStrings.Exception_StreamOperationNotSupport);
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public override void Close() { }
         
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override Task FlushAsync(CancellationToken cancellationToken)
        {
            throw new NotSupportedException(ResourceStrings.Exception_StreamOperationNotSupport);
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        protected override void Dispose(bool disposing) { }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public override void SetLength(long value)
        {
            throw new NotSupportedException(ResourceStrings.Exception_StreamOperationNotSupport);
        }

        #endregion

    }
}
