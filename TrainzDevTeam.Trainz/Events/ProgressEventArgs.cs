﻿using System;

namespace TrainzDevTeam.Trainz
{
    /// <summary>
    /// Аргументы обытия связаного с процессом выполнением действия.
    /// </summary>
    public class ProgressEventArgs : EventArgs
    {

        #region Конструктор

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="ProgressEventArgs"/>.
        /// </summary>
        /// <param name="count">Общее количество обрабатываемых элементов.</param>
        /// <param name="processed">Количество обработанных элементов.</param>
        /// <exception cref="ArgumentException">Аргумент <paramref name="count"/> не может быть меньше 0. -или- Аргумент <paramref name="processed"/> не может быть меньше 0 или больше <paramref name="count"/>.</exception>
        public ProgressEventArgs(long count, long processed)
        {
            if (count < 0) throw new ArgumentOutOfRangeException(nameof(count), ResourceStrings.Exception_ValueCannotNegative);
            if (processed < 0) throw new ArgumentOutOfRangeException(nameof(processed), ResourceStrings.Exception_ValueCannotNegative);
            if (processed > count) throw new ArgumentOutOfRangeException(nameof(processed), ResourceStrings.Exception_ProcessedExceedsCount);
            ItemCount = count;
            ProcessedItemCount = processed;
        }

        #endregion

        #region Свойства

        /// <summary>
        /// Возвращает количество элементов в обработке.
        /// </summary>
        public long ItemCount { get; }

        /// <summary>
        /// Возвращает количество обработанных элементов.
        /// </summary>
        public long ProcessedItemCount { get; private set; }

        /// <summary>
        /// Возвращает значение от 0.0 до 1.0, указывабщее на процесс выполнения обработки.
        /// </summary>
        public float Progress => ProcessedItemCount / (float)ItemCount;

        /// <summary>
        /// Возвращает значение процесса выполнения обработки в процентах.
        /// </summary>
        public int ProgressPercent => (int)(Progress * 100);

        #endregion

        #region Методы

        internal void AddProgress(long count) => ProcessedItemCount += Math.Max(Math.Min(count, ItemCount - ProcessedItemCount), 0);

        #endregion

    }

    /// <summary>
    /// Представляет метод, обрабатывающий обратный вызов для выполнения процесса выполнения действия.
    /// </summary>
    /// <param name="e">Объект, содержащий данные о выполнении действия.</param>
    public delegate void ProgressEventCallback(ProgressEventArgs e);

}
