﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace TrainzDevTeam.Trainz
{
    internal interface IEnumerableList<out T>
    {

        int Version { get; }

        int Count { get; }

        T this[int index] { get; }

    }

    internal struct Enumerator<T> : IEnumerator<T>, IEnumerator
    {

        #region Данные класса

        private IEnumerableList<T> _list;
        private int _index;
        private int _verison;
        private T _current;

        #endregion

        #region Конструткор

        public Enumerator(IEnumerableList<T> list)
        {
            _list = list;
            _index = 0;
            _verison = list.Version;
            _current = default;
        }

        #endregion

        #region Методы

        public bool MoveNext()
        {
            if (_verison == _list.Version && _index < _list.Count) {
                _current = _list[_index++];
                return true;
            } else if (_verison != _list.Version) throw new InvalidOperationException(ResourceStrings.Exception_CollectionFailedVersion);
            _index = _list.Count + 1;
            _current = default;
            return false;
        }

        public void Reset()
        {
            if (_verison != _list.Version) throw new InvalidOperationException(ResourceStrings.Exception_CollectionFailedVersion);
            _index = 0;
            _current = default;
        }

        public void Dispose() { }

        #endregion

        #region Свойства

        public T Current => _current;

        object IEnumerator.Current
        {
            get {
                if (_index == 0 || _index >= _list.Count + 1) throw new InvalidOperationException(ResourceStrings.Exception_EnumerableCantHappen);
                return _current;
            }
        }

        #endregion

    }

}
