﻿using System;
using System.IO;

namespace TrainzDevTeam.Trainz
{
    internal class DataMoveItem : IComparable<DataMoveItem>
    {

        #region Данные класса

        public readonly int Position;
        public readonly int MoveVector;

        #endregion

        #region Конструктор

        public DataMoveItem(int position, int size, int moveVector)
        {
            Position = position;
            MoveVector = moveVector;
            Size = size;
        }

        #endregion

        #region Свойства

        public int Size { get; private set; }

        #endregion

        #region Методы

        public void AddSize(int size) => Size += size;

        public int CompareTo(DataMoveItem two)
        {
            var onemovepos = Position + MoveVector;                     //Позиция в которую будут перемещены данные для первого объекта
            var onemoveendpos = onemovepos + Size - 1;                  //Позиция окончания перемещённых данных для первого объекта
            var oneendpos = Position + Size - 1;                        //Позиция окончания перемещаемых данных для первого объекта
            var twomovepos = two.Position + two.MoveVector;             //Позиция в которую будут перемещены данные для второго объекта
            var twomoveendpos = twomovepos + two.Size - 1;              //Позиция окончания перемещённых данных для второго объекта
            var twoendpos = two.Position + two.Size - 1;                //Позиция окончания перемещаемых данных для второго объекта
            if ((twomovepos >= Position && twomovepos <= oneendpos) || (twomoveendpos >= Position && twomoveendpos <= oneendpos) || (twomovepos <= Position && twomoveendpos >= oneendpos)) return -1;
            if ((onemovepos >= two.Position && onemovepos <= twoendpos) || (onemoveendpos >= two.Position && onemoveendpos <= twoendpos) || (onemovepos <= two.Position && onemoveendpos >= twoendpos)) return 1;
            return 0;
        }

        public static void MoveData(Stream stream, DataMoveItem info)
        {
            var copybuffer = new byte[4194304];
            int copieddatasize = 0;
            while (copieddatasize < info.Size) {
                var copydatasize = Math.Min(info.Size - copieddatasize, copybuffer.Length);
                var dataposition = info.MoveVector > 0 ? info.Position + info.Size - copieddatasize - copydatasize : info.Position + copieddatasize;
                stream.Seek(dataposition, SeekOrigin.Begin);
                stream.Read(copybuffer, 0, copydatasize);
                stream.Seek(dataposition + info.MoveVector, SeekOrigin.Begin);
                stream.Write(copybuffer, 0, copydatasize);
                copieddatasize += copydatasize;
            }
        }

        #endregion

    }
}
