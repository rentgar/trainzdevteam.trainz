﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace TrainzDevTeam.Trainz
{

    /// <summary>
    /// Описывает KUID номер.
    /// </summary>
    public readonly struct Kuid : IEquatable<Kuid>, IComparable<Kuid>, IComparable
    {

        #region Конструктор

        /// <summary>
        /// Инициализирует новый экземпляр структуры <see cref="Kuid"/>.
        /// </summary>
        /// <param name="authorId">Идентификатор автора.</param>
        /// <param name="contentId">Идентификатор контента.</param>
        public Kuid(int authorId, int contentId)
        {
            AuthorId = authorId;
            ContentId = contentId;
            Version = 0;
        }

        /// <summary>
        /// Инициализирует новый экземпляр структуры <see cref="Kuid"/>.
        /// </summary>
        /// <param name="authorId">Идентификатор автора.</param>
        /// <param name="contentId">Идентификатор контента.</param>
        /// <param name="version">Номер версии контента.</param>
        /// <exception cref="ArgumentOutOfRangeException">Аргумент <paramref name="authorId"/> не может иметь отрицательное значение. -или- Аргумент <paramref name="version"/> должен иметь значение от 0 до 127.</exception>
        public Kuid(int authorId, int contentId, byte version)
        {
            if (version > 127) throw new ArgumentOutOfRangeException(nameof(version), ResourceStrings.Exception_KuidVersion);
            if (authorId < 0) throw new ArgumentOutOfRangeException(nameof(authorId), ResourceStrings.Exception_KuidAuthorIdCannotNegative);
            AuthorId = authorId;
            ContentId = contentId;
            Version = version;
        }

        /// <summary>
        /// Инициализирует новый экземпляр структуры <see cref="Kuid"/>.
        /// </summary>
        /// <param name="package">Упакованный в <see cref="long"/> номер KUID.</param>
        public Kuid(long package)
        {
            byte version = (byte)((package >> 24) & byte.MaxValue);
            if (version < byte.MaxValue) {
                AuthorId = (int)(package & 0xFFFFFF);
                Version = (byte)(version / 2);
            } else {
                AuthorId = (int)(package & uint.MaxValue);
                Version = 0;
            }
            ContentId = (int)((int)(package >> 32) & uint.MaxValue);
        }

        #endregion

        #region Методы

        /// <summary>
        /// Запаковывает KUID идентификатор описанный текущим объектом <see cref="Kuid"/> в <see cref="long"/>.
        /// </summary>
        /// <returns><see cref="long"/> значение запакованного KUID идентификатора.</returns>
        public long Packaging()
        {
            long package = 0;
            if (Version > 0 || AuthorId >= 0) {
                package = package | (long)(AuthorId & 0xFFFFFF);
                package = package | ((long)(Version * 2) << 24);
            } else package = (package | (long)AuthorId) & uint.MaxValue;
            package = package | ((long)ContentId << 32);
            return package;
        }

        /// <summary>
        /// Определяет, равен ли заданный объект <see cref="Kuid"/> текущему объекту <see cref="Kuid"/>.
        /// </summary>
        /// <param name="kuid">Объект <see cref="Kuid"/> который необходимо сравнить с текущим объектом <see cref="Kuid"/>.</param>
        /// <returns>Значение <see langword="true"/>, если сравниваемые объекты равны; в противном случае — значение <see langword="false"/>.</returns>
        public bool Equals(Kuid kuid) => AuthorId == kuid.AuthorId && ContentId == kuid.ContentId && Version == kuid.Version;

        /// <summary>
        /// Определяет, равен ли заданный объект текущему объекту.
        /// </summary>
        /// <param name="obj">Объект который необходимо сравнить с текущим объектом.</param>
        /// <returns>Значение <see langword="true"/>, если сравниваемые объекты равны; в противном случае — значение <see langword="false"/>.</returns>
        public override bool Equals(object obj) => obj != null && obj is Kuid && Equals((Kuid)obj);

        /// <summary>
        /// Сравнивает текущий объект <see cref="Kuid"/> с другим объектом <see cref="Kuid"/> и возвращает целое число, которое показывает, 
        /// расположен ли текущий объект перед, после или на той же позиции в порядке сортировки, что и другой объект.
        /// </summary>
        /// <param name="kuid">Объект для сравнения с данным экземпляром.</param>
        /// <returns>
        /// Значение, указывающее, каков относительный порядок сравниваемых объектов. Возвращаемые значения представляют следующие результаты сравнения. 
        /// Значение меньше нуля - данный объект <see cref="Kuid"/> предшествует параметру <paramref name="kuid"/> в порядке сортировки. 
        /// Нуль - в той же позиции в порядке сортировки, что экземпляр <paramref name="kuid"/>. Больше нуля - данный объект <see cref="Kuid"/> 
        /// следует за параметром <paramref name="kuid"/> в порядке сортировки.
        /// </returns>
        public int CompareTo(Kuid kuid)
        {
            int result = (AuthorId < kuid.AuthorId) ? -1 : ((AuthorId > kuid.AuthorId) ? 1 : 0);
            if (result == 0) result = (ContentId < kuid.ContentId) ? -1 : ((ContentId > kuid.ContentId) ? 1 : 0);
            if (result == 0) result = (Version < kuid.Version) ? -1 : ((Version > kuid.Version) ? 1 : 0);
            return result;
        }

        /// <summary>
        /// Возвращает строку, представляющую текущий объект <see cref="Kuid"/>.
        /// </summary>
        /// <returns>Строка, представляющая KUID номер.</returns>
        public override string ToString() => IsNullable ? "NULL" : Version > 0 ? $"kuid2:{AuthorId}:{ContentId}:{Version}" : $"kuid:{AuthorId}:{ContentId}";

        /// <summary>
        /// Возвращает строку, представляющую текущий объект <see cref="Kuid"/>.
        /// </summary>
        /// <param name="bracket">Значение <see langword="true"/>, если номер будет иметь формат &lt;kuid2:XXXXX:YYYYY:ZZZ&gt; (&lt;kuid:XXXXX:YYYYY&gt;); значение <see langword="false"/> — номер будет иметь формат kuid2:XXXXX:YYYYY:ZZZ (kuid:XXXXX:YYYYY).</param>
        /// <returns>Строка, представляющая KUID номер.</returns>
        public string ToString(bool bracket) => bracket ? $"<{ToString()}>" : ToString();

        /// <summary>
        /// Создаёт копию текущего объект <see cref="Kuid"/>.
        /// </summary>
        /// <returns>Объект <see cref="Kuid"/>, который является копией текущего объекта <see cref="Kuid"/>.</returns>
        public Kuid Clone() => new Kuid(AuthorId, ContentId, Version);

        /// <summary>
        /// Возвращает хеш-код для данного объекта <see cref="Kuid"/>
        /// </summary>
        /// <returns>Значение типа <see cref="int"/>, содержащее хеш-код данного объекта.</returns>
        public override int GetHashCode() => Packaging().GetHashCode();

        /// <summary>
        /// Преобразует текущий объект <see cref="Kuid"/> в массив байт.
        /// </summary>
        /// <returns>Массив байт, содержащий данные текущего объекта <see cref="Kuid"/>.</returns>
        public byte[] ToByteArray() => BitConverter.GetBytes(Packaging());

        int IComparable.CompareTo(object obj)
        {
            if (obj == null) return 1;
            if (!(obj is Kuid)) throw new ArgumentException(string.Format(CultureInfo.CurrentCulture, ResourceStrings.Exception_WrongArgumentType, typeof(Kuid).FullName), nameof(obj));
            return CompareTo((Kuid)obj);
        }

        #endregion

        #region Статические методы

        /// <summary>
        /// Преобразует строковое значение KUID в объект <see cref="Kuid"/>. Возвращает значение, указывающее, удачно ли выполнено преобразование.
        /// </summary>
        /// <param name="kuidNumber">Строковое представления KUID.</param>
        /// <param name="result">При возвращении этим методом содержит объект <see cref="Kuid"/>, эквивалентный строковому KUID номеру в параметре <paramref name="kuidNumber"/>, если преобразование выполнено успешно, или <see cref="Kuid.Empty"/>, если оно завершилось неудачей. Этот параметр передается неинициализированным; любое значение, первоначально предоставленное в объекте <paramref name="result"/>, будет перезаписано.</param>
        /// <returns>Значение <see langword="true"/>, если строковое представление было преобразовано в объект <see cref="Kuid"/>; в противном случае — значение <see langword="false"/>.</returns>
        public static bool TryParse(string kuidNumber, out Kuid result)
        {
            result = Empty;
            if (string.IsNullOrWhiteSpace(kuidNumber)) return false;
            kuidNumber = kuidNumber.Trim();
            var regex = new Regex(@"^(?:(?:(?<type>kuid2?):(?<aid>-?[1-9]\d{0,9}):(?<cid>-?[1-9]\d{0,9})(?::(?<ver>[1-9]\d{0,2}|0))?)|(?:<(?<type>kuid2?):(?<aid>-?[1-9]\d{0,9}):(?<cid>-?[1-9]\d{0,9})(?::(?<ver>[1-9]\d{0,2}|0))?>))$", RegexOptions.IgnoreCase | RegexOptions.Singleline | RegexOptions.Compiled);
            var match = regex.Match(kuidNumber);
            if (!match.Success) return false;
            var versionGroup = match.Groups["ver"];
            var iskuid2 = string.Equals(match.Groups["type"].Value, "kuid2", StringComparison.OrdinalIgnoreCase);
            byte version = 0;
            if (iskuid2 != versionGroup.Success) return false;
            if (!int.TryParse(match.Groups["aid"].Value, out var authorId)) return false;
            if (!int.TryParse(match.Groups["cid"].Value, out var contentId)) return false;
            if (versionGroup.Success && !byte.TryParse(versionGroup.Value, out version)) return false;
            if (version > 127 || (version > 0 && authorId < 0)) return false;
            iskuid2 = version > 0;
            result = iskuid2 ? new Kuid(authorId, contentId, version) : new Kuid(authorId, contentId);
            return true;
        }

        /// <summary>
        /// Преобразует строковое значение KUID в объект <see cref="Kuid"/>.
        /// </summary>
        /// <param name="kuidNumber">Строковое представления KUID.</param>
        /// <returns>Объект <see cref="Kuid"/> соответствующий строковому представлению KUID.</returns>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="kuidNumber"/> не может быть <see langword="null"/>.</exception>
        /// <exception cref="FormatException">Строка <paramref name="kuidNumber"/> не является действительным KUID номером.</exception>
        public static Kuid Parse(string kuidNumber)
        {
            if (kuidNumber == null) throw new ArgumentNullException(nameof(kuidNumber));
            if (TryParse(kuidNumber, out var kuid)) return kuid;
            throw new FormatException(ResourceStrings.Exception_KuidFormat);
        }

        /// <summary>
        /// Возвращает объект <see cref="Kuid"/>, преобразованный из 8 байт.
        /// </summary>
        /// <param name="value">Массив байт, содержащий данные объект <see cref="Kuid"/>.</param>
        /// <returns>Объект <see cref="Kuid"/>, преобразованный из 8 байт.</returns>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="value"/> не может быть <see langword="null"/>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Длина массива байт <paramref name="value"/> меньше 8.</exception>
        public static Kuid FromByteArray(byte[] value) => FromByteArray(value, 0);

        /// <summary>
        /// Возвращает объект <see cref="Kuid"/>, преобразованный из 8 байт с указанной позиции в массиве байт.
        /// </summary>
        /// <param name="value">Массив байт, содержащий данные объекта <see cref="Kuid"/>.</param>
        /// <param name="startIndex">Начальная позиция в <paramref name="value"/>.</param>
        /// <returns>Объект <see cref="Kuid"/>, преобразованный из 8 байт с указанной позиции в массиве байт.</returns>
        /// <exception cref="ArgumentNullException">Значение аргумента <paramref name="value"/> не может быть <see langword="null"/>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Длина массива байт <paramref name="value"/> меньше 8.</exception>
        public static Kuid FromByteArray(byte[] value, int startIndex)
        {
            if (value == null) throw new ArgumentNullException(nameof(value));
            if (startIndex < 0 || startIndex + 8 >= value.Length) throw new ArgumentOutOfRangeException(nameof(startIndex));
            return new Kuid(BitConverter.ToInt64(value, startIndex));
        }

        #endregion

        #region Свойства

        /// <summary>
        /// Устарел. Не использовать.
        /// </summary>
        [Obsolete("Больше не используется.", false)]
        public bool IsKUID2 => Version != 0;

        /// <summary>
        /// Возвращает идентификатор автора.
        /// </summary>
        public int AuthorId { get; }

        /// <summary>
        /// Возвращает идентификатор контента.
        /// </summary>
        public int ContentId { get; }

        /// <summary>
        /// Возвращает номер версии контента.
        /// </summary>
        public byte Version { get; }

        /// <summary>
        /// Возвращает хэш значение этого KUID идентификатора.
        /// </summary>
        public byte Hash
        {
            get {
                byte[] octets = ToByteArray();
                byte returnHash = 0;
                foreach (byte octet in octets) returnHash ^= octet;
                if ((octets[3] & 1) == 0) returnHash ^= octets[3];
                return returnHash;
            }
        }

        /// <summary>
        /// Возвращает значение, является ли этот KUID неопределённым (&lt;NULL&gt;).
        /// </summary>
        /// <returns>Значение <see langword="true"/>, если KUID является неопределённым; в противном случае — значение <see langword="false"/>.</returns>
        public bool IsNullable => AuthorId == -1 && ContentId == -1 && Version == 0;

        /// <summary>
        /// Возвращает экземпляр структуры <see cref="Kuid"/>, значение которого равно нулю.
        /// </summary>
        public static Kuid Empty => new Kuid();

        /// <summary>
        /// Возвращает экземпляр структуры <see cref="Kuid"/>, значение которого соответствуют неопределённому KUID (&lt;NULL&gt;).
        /// </summary>
        public static Kuid Nullable => new Kuid(-1);

        #endregion

        #region Операторы

        /// <summary>
        /// Сравнивает два объекта <see cref="Kuid"/>. Результат указывает, равны ли значения свойств <see cref="AuthorId"/>, 
        /// <see cref="ContentId"/> и <see cref="Version"/> двух объектов <see cref="Kuid"/>.
        /// </summary>
        /// <param name="one">Первый объект <see cref="Kuid"/> для сравнения.</param>
        /// <param name="two">Второй объект <see cref="Kuid"/> для сравнения.</param>
        /// <returns>
        /// Значение <see langword="true"/>, если значения свойства <see cref="AuthorId"/>, <see cref="ContentId"/> и <see cref="Version"/> равны; 
        /// в противном случае — значение <see langword="false"/>.
        /// </returns>
        public static bool operator ==(Kuid one, Kuid two) => one.Equals(two);

        /// <summary>
        /// Сравнивает два объекта <see cref="Kuid"/>. Результат указывает, не равны ли значения одного из свойств <see cref="AuthorId"/>, 
        /// <see cref="ContentId"/> и <see cref="Version"/> двух объектов <see cref="Kuid"/>.
        /// </summary>
        /// <param name="one">Первый объект <see cref="Kuid"/> для сравнения.</param>
        /// <param name="two">Второй объект <see cref="Kuid"/> для сравнения.</param>
        /// <returns>
        /// Значение <see langword="true"/>, если значение одного из свойств <see cref="AuthorId"/>, <see cref="ContentId"/>, <see cref="Version"/> не равны; 
        /// в противном случае — значение <see langword="false"/>.
        /// </returns>
        public static bool operator !=(Kuid one, Kuid two) => !one.Equals(two);

        public static bool operator <(Kuid one, Kuid two) => one.CompareTo(two) < 0;

        public static bool operator <=(Kuid one, Kuid two) => one.CompareTo(two) <= 0;

        public static bool operator >(Kuid one, Kuid two) => one.CompareTo(two) > 0;

        public static bool operator >=(Kuid one, Kuid two) => one.CompareTo(two) >= 0;

#pragma warning disable CA2225 

        public static implicit operator long(Kuid kuid) => kuid.Packaging();

        public static explicit operator Kuid(long package) => new Kuid(package);

#pragma warning restore CA2225 

        #endregion

    }
}
